var $ = jQuery;

function KalakeliApp()
{
	/* Application general settings (not user specific) */
	this.settings = new Object();
	this.settings.maxSearchRadius = 30;
	this.settings.transitionTime = 100;

	/* Views object stores all registered application ApView objects */
	this.views = new Object();

	/* Currentyl open, active view */
	this.activeView = null;

	/* In case the menu is closed while a view is active, that view goes into limbo, to be restored when menu is opened again */
	this.limboView = null;
}

/* Do the initialization for application views in control */
KalakeliApp.prototype.initViews = function(toView)
{
	var _this = this;

	this.sidebarNotificationArea = new Element("#sidebar .notification-area");
	this.sidebarNotificationArea.bindIn("sidebarNotification", function(data) { return data.notification; });

	this.contentNotificationArea = new Element("div.content .notification-area");
	this.contentNotificationArea.bindIn("contentNotification", function(data) { return data.notification; });

	this.feedbackNotificationArea = new Element("div#info .notification-area");
	this.feedbackNotificationArea.bindIn("feedbackNotification", function(data) { return data.notification; });

	this.probabilityMeter = new Element("div.catch-probability-meter .catch-probability .value");
	this.probabilityMeter.bindIn("locationProbability", function(data) {

		$("div.catch-probability-meter .catch-probability-filter").css("width", 100 - Math.round(data.score) + "%");
		return Math.round(data.score) + "%";
	});

	this.infoView = new AppView("infoView", "#info");
	this.infoView.add("feedbackName", "#feedback-name", true);
	this.infoView.add("feedbackEmail", "#feedback-email", true);
	this.infoView.add("feedbackMessage", "#feedback-message", true);
	this.infoView.add("feedbackSubmit", "#feedback-submit", true);

	this.locationSearchView = new AppView("locationSearchView", "div.view-location-search");
	this.bindView(this.locationSearchView, ".view-location-search-deploy", "div.view-location-search .close-view");
	this.locationSearchView.add("locationSearchPlace", "#location-search-place", true);
	this.locationSearchView.withdraw(0);

	this.weatherView = new AppView("weatherView", "div.view-weather");
	this.bindView(this.weatherView, ".view-weather-deploy", "div.view-weather .close-view");
	this.weatherView.add("weatherLocation", "#weather-location", true);
	this.weatherView.weatherLocation.element.on("change", function(ev) {

		var place = $(ev.currentTarget).find(":selected");
		var coords = {};
		if (place.val() === "map") {

			$(document).trigger("weatherInfoFromMap");
			$(".change-weather-map-loc").removeClass("hidden");
		}
		else {

			$(".change-weather-map-loc").addClass("hidden");
			if (place.val() !== "current") {

				coords = {"lat" : place.attr("data-lat"), "lon" : place.attr("data-lon")};
			}
			$(document).trigger("updateWeatherData", coords);
		}
	});
	this.weatherView.add("weatherData", "#weather-data", false);
	this.weatherView.weatherData.bindIn("weatherData", function(v) {

		var mapWeatherSymbol = function(code){
    
		    code = parseInt(code);
		    
		    if (!code) {
		        return 1;
		    }
		    
		    var mapping = {
		        1: [2], // half cloudy
		        2: [3, 91, 92], // cloudy or hazy
		        3: [1], // clear
		        4: [21,22,23,31,32,33], //rain
		        5: [41,42,43,51,52,53,71,72,73,81,82,83], //snowing 
		        6: [61, 62, 63, 64], //lightning
		    }
		    for(k in mapping){
		        if ($.inArray(code, mapping[k]) !== -1){
		            return k;
		        }
		    }
		    return 1;
		}
		var list = $('<ul>');
		var referenceTime = new Date();
		referenceTime.setHours((new Date(v.weatherData[0].data[0].time)).getHours());
		referenceTime.setMilliseconds(0);
		referenceTime.setSeconds(0);
		referenceTime.setMinutes(0);

		var days = ['Sunnuntai', 'Maanantai', 'Tiistai', 'Keskiviikko', 'Torstai', 'Perjantai', 'Lauantai'];
		var lastDate = 0;
		for (var i = 0; i < 18; i++) {

			if (lastDate !== referenceTime.getDate()) {

				list.append($('<li class="weather-date">').html(days[referenceTime.getDay()] + ' ' + referenceTime.getDate() + '.' + (referenceTime.getMonth() + 1) + '.'));
				lastDate = referenceTime.getDate();
			}
			list.append($('<li class="weather-data">').attr('data-time', referenceTime.toISOString()).html('<span class="time">' + referenceTime.getHours() + ':00</span><span class="weather-symbol"></span><span class="wind-direction"><i class="fa fa-long-arrow-up" style="display: inline-block;"></i></span><span class="wind-speed"></span><span class="temperature"></span>')); // fix this, it's ugly
			referenceTime.setHours(referenceTime.getHours() + 2);
		}

		$.each(v.weatherData, function(i, set) {

			if (set.id === "Temperature") {

				$.each(set.data, function(j, instance) {
					list.find('li[data-time="' + (new Date(Date.parse(instance.time))).toISOString() + '"] .temperature').html(Math.round(parseFloat(instance.value)) + " &deg;c");
				});
			}
			else if (set.id === "WindSpeedMS") {

				$.each(set.data, function(j, instance) {

					list.find('li[data-time="' + (new Date(Date.parse(instance.time))).toISOString() + '"] .wind-speed').html(Math.round(parseFloat(instance.value)) + " m/s");
				});
			}
			else if (set.id === "WindDirection") {

				$.each(set.data, function(j, instance) {

					var deg = Math.round(parseFloat(instance.value));
					var rad = deg * (Math.PI/180);
					var cos = Math.cos(rad);
					var sin = Math.sin(rad);
					list.find('li[data-time="' + (new Date(Date.parse(instance.time))).toISOString() + '"] .wind-direction i')
						.attr('style',
								'-webkit-transform: rotate(' + deg + 'deg);'+
								'-moz-transform: rotate(' + deg + 'deg);'+
								'-ms-transform: rotate(' + deg + 'deg);'+
								'-o-transform: rotate(' + deg + 'deg);'+
								'transform: rotate(' + deg + 'deg);'+
								/*'filter: progid:DXImageTransform.Microsoft.Matrix(sizingMethod="auto expand", M11=' + cos + ', M12=-' + sin + ', M21=' + sin + ', M22=' + cos + ');'+*/
								'-ms-filter: "progid:DXImageTransform.Microsoft.Matrix(M11=' + cos + ', M12=' + (-1 * sin) + ', M21=' + sin + ', M22=' + cos + ', SizingMethod=\'auto expand\')";'
							);
							

				});
			}
			else if (set.id === "WeatherSymbol3") {
				
				$.each(set.data, function(j, instance) {
					
					var symbolIcon = $('<img />');

					var iconID = mapWeatherSymbol(instance.value);
					symbolIcon.attr('src', 'images/weather/' + iconID + '.png');
				
					list.find('li[data-time="' + (new Date(Date.parse(instance.time))).toISOString() + '"] .weather-symbol').append(symbolIcon);
				});
			}
		});

		return list;
	});
	this.weatherView.withdraw(0);

	/* ADDCATCH VIEW */
	this.addCatchView = new AppView("addCatchView", "div.view-add-catch");
	this.bindView(this.addCatchView, ".view-add-catch-deploy", "div.view-add-catch .close-view");

	this.addCatchView.add("typeBiggestFishValue", "[name='type_biggestfish']", false);
	this.addCatchView.add("typeFish15Value", "[name='type_fish15']", false);
	this.addCatchView.add("typeOwnValue", "[name='type_own']", false);
	this.addCatchView.add("token", "#addcatch-token", false);
	this.addCatchView.add("emailNotification", "#addcatch-emailnotification", false);
	this.addCatchView.add("lat", "#addcatch-lat", false);
	this.addCatchView.lat.bindIn("addCatchLocationChange", function(v) {

		if (v === "current") {

			if (typeof AppData.User.geoLocation === "object" && AppData.User.geoLocation.coords === "object") {

				return AppData.User.geoLocation.coords.latitude;
			}
			else {

				$(document).one("userGeoLocation", function() {

					_this.addCatchView.lat.set(AppData.User.geoLocation.coords.latitude);
				});
				$(document).trigger("userLocationNeeded");
			}
		}
		else if (v === "none") {

			return "";
		}
		else if (v === "map") {

			return _this.addCatchView.lat.get();
		}
		else {

			return _this.addCatchView.addCatchLocation.element.find('[value="'+v+'"]').attr('data-lat');
		}
	});
	this.addCatchView.add("lon", "#addcatch-lon", false);
	this.addCatchView.lon.bindIn("addCatchLocationChange", function(v) {

		if (v === "current") {

			if (typeof AppData.User.geoLocation === "object" && AppData.User.geoLocation.coords === "object") {

				return AppData.User.geoLocation.coords.longitude;
			}
			else {

				$(document).one("userGeoLocation", function() {

					_this.addCatchView.lon.set(AppData.User.geoLocation.coords.longitude);
				});
				$(document).trigger("userLocationNeeded");
			}
		}
		else if (v === "none") {

			return "";
		}
		else if (v === "map") {

			return _this.addCatchView.lon.get();
		}
		else {

			return _this.addCatchView.addCatchLocation.element.find('[value="'+v+'"]').attr('data-lon');
		}
	});
	this.addCatchView.add("catcher", "#addcatch-catcher", false);
	this.addCatchView.add("cr", "#addcatch-cr", false);
	this.addCatchView.add("crValue", "[name='cr']", false);
	this.addCatchView.add("species", "#addcatch-species", false);
	this.addCatchView.add("method", "#addcatch-method", false);
	this.addCatchView.add("bait", "#addcatch-bait", false);
	this.addCatchView.add("weight", "#addcatch-weight", false);
	this.addCatchView.add("addCatchImage1", "#addcatch-catchimage1", true);
	this.addCatchView.add("addCatchImage2", "#addcatch-catchimage2", true);
	this.addCatchView.add("addCatchBaitImage", "#addcatch-baitimage", true);

	this.addCatchView.add("weightKilosValue", ".addcatch-weight-kilos-slider-value", false);
	this.addCatchView.add("weightKilosSlider", ".addcatch-weight-kilos-slider", true);
	this.addCatchView.weight.bindIn("weightKilosSliderChange", function(v) {

		var current = _this.addCatchView.weight.element.val();
		var parts = current.split(".");
		var next = v + "." + parts[1];

		return next;
	});
	this.addCatchView.weight.bindIn("weightKilosSliderLoad", function(v) {

		var current = _this.addCatchView.weight.element.val();
		var parts = current.split(".");
		var next = v + "." + parts[1];

		return next;
	});
	this.addCatchView.weightKilosValue.bindIn("weightKilosSliderChange");
	this.addCatchView.weightKilosValue.bindIn("weightKilosSliderLoad");

	this.addCatchView.add("weightHektosValue", ".addcatch-weight-hektos-slider-value", false);
	this.addCatchView.add("weightHektosSlider", ".addcatch-weight-hektos-slider", true);
	this.addCatchView.weight.bindIn("weightHektosSliderChange", function(v) {

		var current = _this.addCatchView.weight.element.val();
		var parts = current.split(".");
		var next = parts[0] + "." + v;

		return next;
	});
	this.addCatchView.weight.bindIn("weightHektosSliderLoad", function(v) {

		var current = _this.addCatchView.weight.element.val();
		var parts = current.split(".");
		var next = parts[0] + "." + v;

		return next;
	});
	this.addCatchView.weightHektosValue.bindIn("weightHektosSliderChange");
	this.addCatchView.weightHektosValue.bindIn("weightHektosSliderLoad");

	this.addCatchView.add("catchLength", "#addcatch-length", false);
	this.addCatchView.add("lengthValue", ".addcatch-length-slider-value", false);
	this.addCatchView.add("lengthSlider", ".addcatch-length-slider", true);
	this.addCatchView.catchLength.bindIn("lengthSliderChange");
	this.addCatchView.catchLength.bindIn("lengthSliderLoad");
	this.addCatchView.lengthValue.bindIn("lengthSliderChange");
	this.addCatchView.lengthValue.bindIn("lengthSliderLoad");
	this.addCatchView.add("addCatchLocation", "#addcatch-location", true);
	this.addCatchView.add("location", "#addcatch-location-name", true);
	this.addCatchView.add("weather", "#addcatch-weather", true);
	this.addCatchView.add("story", "#addcatch-story", true);
	this.addCatchView.add("cr", "#addcatch-cr", true);
	this.addCatchView.location.bindIn("addCatchLocationChange", function(v) {

		if (v !== "none" && v !== "current" && v !== "map") {

			return v;
		}
		else {

			return "";
		}
	});
	this.addCatchView.add("date", "#addcatch-date", false);
	this.addCatchView.date.element.datetimepicker({
		'lang' : 'fi',
		'format' : 'd-m-Y',
		'timepicker' : false,
	});
	this.addCatchView.add("time", "#addcatch-time", false);
	this.addCatchView.time.element.datetimepicker({
		'lang' : 'fi',
		'step' : 1,
		'format' : 'H:i',
		'datepicker' : false,
	});
	this.addCatchView.add("submit", "#addcatch-submit", true);
	this.addCatchView.withdraw(0);

	/* User views */

	this.signinView = new AppView("signinView", "div.view-user-signin");
	this.bindView(this.signinView, ".view-user-deploy", "div.view-user-signin .close-view");
	this.signinView.withdraw(0);
	this.signinView.add('userEmail', '#signin-email', false);
	this.signinView.add('userPassword', '#signin-password', false);

	this.profileView = new AppView("profileView", "div.view-user-profile");
	this.profileView.add("ownPlaces", "#own-places tbody", false);
	this.profileView.add("ownCatchlist", "#own-catchlist tbody", false);
	this.profileView.add("toggleEmailNotifications", "#toggleEmailNotifications", false);
	this.profileView.withdraw(0);

	this.userLabel = new Element(".user-label");
	this.userLabel.bindIn('userSignedIn', function(v) {

		var label = "Profiili";
		if (!!v.first_name) {

			label = v.first_name;
		}
		if (!!v.last_name) {

			label += " " + v.last_name;
		}
		return label;
	});
	if (!!toView) {

		this.deployView(toView);
	}

	/* Baitwizard */
	this.baitwizardView = new AppView("baitwizardView", "div.view-baitwizard");
	this.bindView(this.baitwizardView, ".view-baitwizard-deploy", "div.view-baitwizard .close-view");
	this.baitwizardView.withdraw(0);

	this.map = new Element("#map");

}

KalakeliApp.prototype.attachListeners = function()
{

	var _this = this;

	/*  APP LISTENERS  */

	var handleMessage = function(message) {

		var data = null
		try {

			data = $.parseJSON(message.data);
		}
		catch(err) {

			return false;
		}

		switch(data.event) {

			case "addcatch":
				if (data.success === true) {

					$(document).trigger("addCatchSuccess", data);
				}
				else {

					_this.notify("Saalin lisäys epäonnistui.", "error", "content");
				}
				break;

			case "feedback":
				if (data.success === true) {

					$(document).trigger("feedbackSuccess", data);
				}
				else {

					_this.notify("Palautetta ei voitu lähettää.", "error", "feedback");
				}
		}
	}
	if (window.addEventListener) {
		window.addEventListener("message", handleMessage, false);
	}
	else if (window.attachEvent) {
		window.attachEvent("onmessage", handleMessage);
	}

	$(document).on("needSignIn", function(e, v) {

		if (typeof v === "undefined") {

			v = {};
		}

		_this.deployView("signinView");
		if (typeof v.redirectTo !== "undefined") {

			if (_this.isView(v.redirectTo)) {

				$(document).one('userSignedIn', function() {

					_this.deployView(v.redirectTo);
				});
			}
		}
	});
	
	$(document).on("registrationSuccess", function(e, v) {

		_this.notify("Sinulle on luotu käyttäjätili Erän sivustolle. Sinulle on lähetetty sähköpostia, josta löydät salasanasi.", "success", "sidebar");
	});

	$(document).on("registrationFailed", function(e, v) {

		_this.notify("Rekisteröityminen epäonnistui. Varmistathan, että antamasi sähköposti ei ole jo rekisteröity Erän sivustolle.", "error", "content");
	});

	$(document).on("locationAdded", function() {

		_this.notify("Paikka tallennettu.", "success", "sidebar");
	});

	$(document).on("locationDeleted", function() {

		_this.notify("Paikka poistettu.", "success", "sidebar");
	});

	$(".help-deploy").on("click", function() {

		$("#info").toggleClass("hidden");
	});

	$(".close-info").on("click", function() {

		$("#info").addClass("hidden");
	});

	$(document).on("newUser", function() {

		$("#info").removeClass("hidden");
	});

	var snapper = new Snap({
		element: document.getElementById('snap-container'),
		tapToClose: false,
	})
	.on('close', function() {

		if ($("body").hasClass("wide") || $("body").hasClass("normal")) {

			$(".all-elements").css({"width" : "100%", "width" : "100vw"});
		}

		$("#sidebar").attr("data-state", "closed");
		_this.contentNotificationArea.set("");
		_this.sidebarNotificationArea.set("");

		if (!!_this.activeView) {

			_this.limboView = _this.activeView;
			_this.activeView = null;
		}
		for (v in _this){
			if (_this.hasOwnProperty(v) && _this.isView(v)) 
				_this[v].withdraw(_this.settings.transitionTime);
		}
	})
	.on('open', function() {

		if ($("body").hasClass("wide") || $("body").hasClass("normal")) {

			$(".all-elements").css("width", $(window).width() - 348);
		}

		$("#sidebar").attr("data-state", "open");
		$("#info").addClass("hidden");
		if (!!_this.limboView) {

			_this.deployView(_this.limboView);
			_this.limboView = null;
		}
	});

	$(document).on('openNav', function(e, v) {

		snapper.open('left');
	});

	/*  TEMPLATE LISTENERS  */
	$('.close-nav, .sidebar-close, .shortcut-close').click(function(){
		$(this).blur();
		snapper.close();
	});

	$('.open-nav').click(function () {
		$(this).blur();
		if (snapper.state().state == "left" || $("#sidebar").attr("data-state") === "open") {
			snapper.close();
		}
		else {
			snapper.open('left');
		}
	});

	$('.deploy-sidebar').click(function(){
		$(this).blur();
		if (snapper.state().state=="left" || $("#sidebar").attr("data-state") === "open") {
			snapper.close();
		}
		else {
			snapper.open('left');
		}
	});

	$(".notification-area").on("click", ".tap-dismiss-notification", function(){

		$(this).remove();
		return false;
	});

	/*
	 *
	 * Info view
	 **/
	$(document).on("feedbackSubmitChange", function(e, v) {

		var data = {
			"name" : _this.infoView.feedbackName.get(),
			"email" : _this.infoView.feedbackEmail.get(),
			"message" : _this.infoView.feedbackMessage.get(),
		}
		$(document).trigger("sendFeedback", data);
	});

 	$(document).on("feedbackSuccess", function(e, v) {

 		_this.infoView.feedbackName.set("");
 		_this.infoView.feedbackEmail.set("");
 		_this.infoView.feedbackMessage.set("");
 		_this.notify("Kiitos palautteestasi.", "success", "feedback");
 	});
 	$(document).on("feedbackError", function(e, v) {

 		if (typeof v === "object") {
 			if (!!v.message) {
 				return _this.notify(v.message, "warning", "feedback");
 			}
 		}
 		_this.notify("Palautetta ei voitu lähettää.", "warning", "feedback");
 	});

	/*
	 * Location search view */

	$("#location-search-form").on("submit", function(ev) {

		ev.preventDefault();
		$(document).trigger("searchLocation", {"place" : $("#location-search-place").val()});
	});

	$("#location-search-form .submit").on("click", function(ev) {

		ev.preventDefault();
		$(document).trigger("searchLocation", {"place" : $("#location-search-place").val()});
	});

	$("#location-search-place").on("keydown", function(ev) {

		window.setTimeout(function() {

			$(document).trigger("searchLocation", {"place" : $(ev.currentTarget).val()});
		}, 50);
	});

	$(document).on("locationSearchResults", function(e, v) {

		var results = $("#location-search-results tbody");
		results.html("");
		for (place in v.data) {

			results.append(
			'<tr>'+
				'<td class="table-sub-title">'+ v.data[place].properties.title +'</td>'+
				'<td>'+
				'<a data-toggle="map-marker" href="#" data-name="'+ v.data[place].properties.title +'" data-lat="'+ v.data[place].geometry.coordinates[1] +'" data-lon="'+ v.data[place].geometry.coordinates[0] +'">'+
				'<i class="fa fa-map-marker"></i>'+
				'</a>'+
				'</td>'+
			'</tr>'
			);
		}

		if (v.data.length == 0 && $("#location-search-place").val() !== "") {

			results.append(
			'<tr>'+
				'<td style="text-align: center;">Valitettavasti hakemaasi paikkaa ei löydy.<br>Haluatko etsiä sen kartalta?<br><br><button onclick="$(\'.close-nav\').click()" class="button button-blue">Näytä kartta</a></td>'+
			'</tr>'
			);
		}
	});

	$("#location-search-results").on("click", "a", function(ev) {

		ev.preventDefault();
		var place = $(ev.currentTarget);
		var data = {"name" : place.attr("data-name"), "lat" : place.attr("data-lat"), "lon" : place.attr("data-lon"), "type" : "location-search"};
		$(document).trigger("setPlaceMarker", data);
		if (snapper.state().state === "left" && !$("body").hasClass("wide")) {
			snapper.close();
		}
	});

	/*
	 * Add catch view */

	$('#addcatch-cr').on('click', function(ev) {

		$(ev.currentTarget).toggleClass('checkbox-three-checked');
		if ($(ev.currentTarget).hasClass('checkbox-three-checked')) {

			_this.addCatchView.crValue.set('true');
		}
		else {

			_this.addCatchView.crValue.set('false');			
		}
	});

	$('#catchtypeBiggestFish').on('click', function(ev) {

		$(ev.currentTarget).toggleClass('checkbox-three-checked');
		if ($(ev.currentTarget).hasClass('checkbox-three-checked')) {

			$("[data-toggle='#catchtypeBiggestFish']").removeClass('hidden');
			_this.addCatchView.typeBiggestFishValue.set('true');
		}
		else {

			$("[data-toggle='#catchtypeBiggestFish']").find('input').val('');
			$("#biggestFishTwoContestants").removeClass('checkbox-three-checked');
			$("[data-toggle='#biggestFishTwoContestants']").addClass('hidden');
			$("#biggestFishTwoWitnesses").removeClass('checkbox-three-checked');
			$("[data-toggle='#biggestFishTwoWitnesses']").addClass('hidden');
			$("[data-toggle='#catchtypeBiggestFish']").addClass('hidden');
			_this.addCatchView.typeBiggestFishValue.set('false');
		}
	});

	$('#catchtypeFish15').on('click', function(ev) {

		$(ev.currentTarget).toggleClass('checkbox-three-checked');
		if ($(ev.currentTarget).hasClass('checkbox-three-checked')) {

			_this.addCatchView.typeFish15Value.set('true');
		}
		else {

			_this.addCatchView.typeFish15Value.set('false');			
		}
	});

	$('#catchtypeOwn').on('click', function(ev) {

		$(ev.currentTarget).toggleClass('checkbox-three-checked');
		if ($(ev.currentTarget).hasClass('checkbox-three-checked')) {

			_this.addCatchView.typeOwnValue.set('true');
		}
		else {

			_this.addCatchView.typeOwnValue.set('false');			
		}
	});

	$('#biggestFishTwoContestants').on('click', function(ev) {

		$(ev.currentTarget).toggleClass('checkbox-three-checked');
		if ($(ev.currentTarget).hasClass('checkbox-three-checked')) {

			$("[data-toggle='#biggestFishTwoContestants']").removeClass('hidden');
		}
		else {

			$("[data-toggle='#biggestFishTwoContestants']").find('input').val('');
			$("[data-toggle='#biggestFishTwoContestants']").addClass('hidden');	
		}
	});

	$('#biggestFishTwoWitnesses').on('click', function(ev) {

		$(ev.currentTarget).toggleClass('checkbox-three-checked');
		if ($(ev.currentTarget).hasClass('checkbox-three-checked')) {

			$("[data-toggle='#biggestFishTwoWitnesses']").removeClass('hidden');
		}
		else {

			$("[data-toggle='#biggestFishTwoWitnesses']").find('input').val('');
			$("[data-toggle='#biggestFishTwoWitnesses']").addClass('hidden');
		}
	});

	$(".addcatch-length-slider").noUiSlider({
        start: 5.0,
        connect: "lower",
        step: 0.5,
        range: {min: 5.0, max: 130.0}
    }).Link('lower').to('-inline-<div class="tooltip top"></div>', function ( value ) {

		$(this).html(
			'<div class="tooltip-arrow" style="left: 50%;"></div>'+
			'<div class="tooltip-inner">'+value+'</div>'
		);
	});

	$(".addcatch-length-slider-minus").on("click", function(ev) {

		ev.preventDefault();
		$(".addcatch-length-slider").val(parseFloat($(".addcatch-length-slider").val()) - 0.5);
		$(document).trigger("lengthSliderChange", $(".addcatch-length-slider").val());
	});

	$(".addcatch-length-slider-plus").on("click", function(ev) {

		ev.preventDefault();
		$(".addcatch-length-slider").val(parseFloat($(".addcatch-length-slider").val()) + 0.5);
		$(document).trigger("lengthSliderChange", $(".addcatch-length-slider").val());
	});

	$(".addcatch-weight-kilos-slider").noUiSlider({
        start: 1,
        connect: "lower",
        range: {min: 0, max: 30},
        format: {
		  to: function (value) {
			return Math.round(value);
		  },
		  from: function (value) {
			return Math.round(value);
		  }
		}
    }).Link('lower').to('-inline-<div class="tooltip top"></div>', function ( value ) {

		$(this).html(
			'<div class="tooltip-arrow" style="left: 50%;"></div>'+
			'<div class="tooltip-inner">'+value+'</div>'
		);
	});

	$(".addcatch-weight-kilos-slider-minus").on("click", function(ev) {

		ev.preventDefault();
		$(".addcatch-weight-kilos-slider").val($(".addcatch-weight-kilos-slider").val() - 1);
		$(document).trigger("weightKilosSliderChange", $(".addcatch-weight-kilos-slider").val());
	});

	$(".addcatch-weight-kilos-slider-plus").on("click", function(ev) {

		ev.preventDefault();
		$(".addcatch-weight-kilos-slider").val($(".addcatch-weight-kilos-slider").val() + 1);
		$(document).trigger("weightKilosSliderChange", $(".addcatch-weight-kilos-slider").val());
	});

	$(".addcatch-weight-hektos-slider").noUiSlider({
        start: 0,
        connect: "lower",
		step: 50,
		range: {
			'min': [  0 ],
			'max': [ 950 ]
		},
        format: {
		  to: function (value) {
			return Math.round(value);
		  },
		  from: function (value) {
			return Math.round(value);
		  }
		}
    }).Link('lower').to('-inline-<div class="tooltip top"></div>', function ( value ) {

		$(this).html(
			'<div class="tooltip-arrow" style="left: 50%;"></div>'+
			'<div class="tooltip-inner">'+value+'</div>'
		);
	});

	$(".addcatch-weight-hektos-slider-minus").on("click", function(ev) {

		ev.preventDefault();
		$(".addcatch-weight-hektos-slider").val($(".addcatch-weight-hektos-slider").val() - 50);
		$(document).trigger("weightHektosSliderChange", $(".addcatch-weight-hektos-slider").val());
	});

	$(".addcatch-weight-hektos-slider-plus").on("click", function(ev) {

		ev.preventDefault();
		$(".addcatch-weight-hektos-slider").val($(".addcatch-weight-hektos-slider").val() + 50);
		$(document).trigger("weightHektosSliderChange", $(".addcatch-weight-hektos-slider").val());
	});

	$(document).on("userProfile", function(e, v) {

		var options = '<option value="none" selected>Valitse sijainti</option>';
		    options += '<option value="current">Oma sijainti</option>';
		    options += '<option value="map">Sijainti kartalta</option>';

		if (!!v.places) {

			for (var i in v.places) {

				var place = v.places[i];
				options += '<option value="'+place.name+'" data-lat="'+place.coords[0]+'" data-lon="'+place.coords[1]+'">'+place.name+'</option>';
			}
		}
		_this.addCatchView.addCatchLocation.element.html(options);

		if (typeof v.emailNotifications !== "undefined") {

			if (v.emailNotifications === false) {

				_this.addCatchView.emailNotification.set("0");
			}
		}
	});

	$(document).on("addCatchImage1Change", function(e, v) {

        var reader = new FileReader();

        reader.onload = function (e) {
            var preview = $('<img/>').attr('id', 'addcatchimage1-preview').attr('src', e.target.result).css("max-width", "100%");
            _this.addCatchView.addCatchImage1.element.parent().parent().find('img#addcatchimage1-preview').remove();
            _this.addCatchView.addCatchImage1.element.parent().after(preview);
        }

        reader.readAsDataURL($("#addcatch-catchimage1")[0].files[0]);
	});

	$(document).on("addCatchImage2Change", function(e, v) {

        var reader = new FileReader();

        reader.onload = function (e) {
            var preview = $('<img/>').attr('id', 'addcatchimage2-preview').attr('src', e.target.result).css("max-width", "100%");
            _this.addCatchView.addCatchImage2.element.parent().parent().find('img#addcatchimage2-preview').remove();
            _this.addCatchView.addCatchImage2.element.parent().after(preview);
        }

        reader.readAsDataURL($("#addcatch-catchimage2")[0].files[0]);
	});

	$(document).on("addCatchBaitImageChange", function(e, v) {

        var reader = new FileReader();

        reader.onload = function (e) {
            var preview = $('<img/>').attr('id', 'addcatchbaitimage-preview').attr('src', e.target.result).css("max-width", "100%");
            _this.addCatchView.addCatchBaitImage.element.parent().parent().find('img#addcatchbaitimage-preview').remove();
            _this.addCatchView.addCatchBaitImage.element.parent().after(preview);
        }

        reader.readAsDataURL($("#addcatch-baitimage")[0].files[0]);
	});

	$(document).on('addCatchLocationChange', function(e, v) {

		if (v === "map") {

			$(document).one("mapCoords", function(e, v) {

				snapper.open('left');
				_this.addCatchView.lat.set(v.coords.lat);
				_this.addCatchView.lon.set(v.coords.lng);
			});
			snapper.close();
		}
	});

	$("#addcatch-form").on("submit", function(ev) {

		if (_this.addCatchView.submit.element.attr("data-status") === "working") {

			return false;
		}
		_this.addCatchView.submit.element.attr("data-status", "working");
		_this.contentNotificationArea.set("");
		_this.addCatchView.parentElement.find(".addcatch-species-warning").remove();
		_this.addCatchView.parentElement.find(".addcatch-email-warning").remove();
		var form = $(ev.currentTarget);

		if (!_this.addCatchView.species.get()) {

			ev.preventDefault();
			_this.addCatchView.submit.element.attr("data-status", "idle");
			_this.notify("Valitse kalalaji.", "warning", "content");
			_this.addCatchView.species.element.before("<span class='addcatch-species-warning'>Valitse kalalaji</span>");

			return false;
		}
		if (!_this.addCatchView.token.get()) {
			
			ev.preventDefault();
			return false;
		}
	});

	$(document).on("addCatchSuccess", function(e, v) {

		_this.addCatchView.submit.element.attr("data-status", "idle");

		// reset all textfields and textareas
		$('#addcatch-form input[type="text"]').val('');
		$('#addcatch-form textarea').html('').val('');

		// reset all checkboxes and corresponding value inputs to false
		$('#addcatch-form .checkbox-three').removeClass('checkbox-three-checked');
		$('#addcatch-form [data-default="false"]').val('false');

		// hide what need be
		$('#addcatch-form [data-default="hidden"]').addClass('hidden');

		// reset species to 'none'
		_this.addCatchView.species.element.find("option").prop("selected", false);
		_this.addCatchView.species.element.find("option[rel='-1']").prop("selected", true);

		// reset catch method to 'none'
		_this.addCatchView.method.element.find("option").prop("selected", false);
		_this.addCatchView.method.element.find("option[rel='0']").prop("selected", true);

		// reset catch's physical dimensions
		_this.addCatchView.weight.set("1.000");
		_this.addCatchView.catchLength.set("1");
		_this.addCatchView.weightKilosSlider.set(1);
		_this.addCatchView.weightHektosSlider.set(0);
		_this.addCatchView.lengthSlider.set(5.00);
		_this.addCatchView.weightKilosValue.set(1);
		_this.addCatchView.weightHektosValue.set(0);
		_this.addCatchView.lengthValue.set(5.00);

		// reset imagefields
		var imageField = _this.addCatchView.addCatchImage1.element.clone(true);
		_this.addCatchView.addCatchImage1.element.replaceWith(imageField);
		_this.addCatchView.addCatchImage1.element = imageField;
		
		imageField = _this.addCatchView.addCatchImage2.element.clone(true);
		_this.addCatchView.addCatchImage2.element.replaceWith(imageField);
		_this.addCatchView.addCatchImage2.element = imageField;
		
		imageField = _this.addCatchView.addCatchBaitImage.element.clone(true);
		_this.addCatchView.addCatchBaitImage.element.replaceWith(imageField);
		_this.addCatchView.addCatchBaitImage.element = imageField;

		_this.addCatchView.addCatchImage1.element.parent().parent().find('img').remove();
		
		// reset location to 'none'
		_this.addCatchView.addCatchLocation.element.find("option").prop("selected", false);
		_this.addCatchView.addCatchLocation.element.find("option[value='none']").prop("selected", true);
		_this.addCatchView.location.set("");
		var lat = _this.addCatchView.lat.get();
		_this.addCatchView.lat.set("");
		var lon = _this.addCatchView.lon.get();
		_this.addCatchView.lon.set("");

		// close form and notify user
		_this.addCatchView.withdraw(0);
		_this.notify("Saalis lisätty.", "success", "sidebar");

		// place marker
		if (!!lat && !!lon) {

			$(document).one("userCatchlist", function() {

				if (snapper.state().state === "left") {
					snapper.close();
				}
				$(document).trigger("setCatchMarker", {"ownCatch" : true, "id" : v.post_id});
			});
			$(document).trigger("updateOwnCatches");
		}
	});

	$(document).on("addCatchToLocation", function(e, v) {

		if (snapper.state().state !== "left") {
			snapper.open("left");
		}
		_this.deployView(_this.addCatchView);

		_this.addCatchView.addCatchLocation.element.find('option').prop('selected', false);
		if (!!v.name && v.name !== "undefined") {

			if (v.name === "Oma sijainti") {

				_this.addCatchView.addCatchLocation.element.find('[value="current"]').prop('selected', true);
			}
			else if (!!_this.addCatchView.addCatchLocation.element.find('[value="'+v.name+'"]').length) {

				_this.addCatchView.addCatchLocation.element.find('[value="'+v.name+'"]').prop('selected', true);
				_this.addCatchView.addCatchLocation.element.change();
			}
			else {

				_this.addCatchView.addCatchLocation.element.find('[value="map"]').prop('selected', true);
				_this.addCatchView.lat.set(v.lat);
				_this.addCatchView.lon.set(v.lon);
				_this.addCatchView.location.set(v.name);
			}
		}
		else {
			_this.addCatchView.addCatchLocation.element.find('[value="map"]').prop('selected', true);
			_this.addCatchView.lat.set(v.lat);
			_this.addCatchView.lon.set(v.lon);
			_this.addCatchView.location.set("");
		}
	});


	/*
	 * Weather view */
	$(document).on("userProfile", function(e, v) {

		var options = '<option value="none" selected>Valitse sijainti</option>';
		options += '<option value="current">Oma sijainti</option>';
		options += '<option value="map">Sijainti kartalta</option>';
		for (var i in v.places) {

			var place = v.places[i];
			options += '<option data-lat="'+ place.coords[0] +'" data-lon="'+ place.coords[1] +'" value="'+ place.name +'">'+ place.name +'</option>';
		}
		_this.weatherView.weatherLocation.element.html(options);
	});

	$(document).on("showWeather", function(e, v) {

		if (_this.activeView !== _this.weatherView) {

			if (snapper.state().state !== "left") {
				snapper.open("left");
			}
			_this.deployView(_this.weatherView);
		}
		_this.weatherView.weatherLocation.element.find("option").prop('selected', false);
		if (!!v.name && v.name !== 'undefined') {

			$(".change-weather-map-loc").addClass("hidden");
			if (_this.weatherView.weatherLocation.element.find("[value='"+ v.name +"']").prop('selected', true).length == 0) {

				$(".change-weather-map-loc").removeClass("hidden");
				if (v.name === "Oma sijainti") {

					$(".change-weather-map-loc").addClass("hidden");
					_this.weatherView.weatherLocation.element.find("[value='current']").prop('selected', true);	
				}
				else {

					_this.weatherView.weatherLocation.element.find("[value='map']").prop('selected', true);
					_this.weatherView.weatherLocation.element.find("[value='map']").html('Sijainti kartalta: ' + v.name);
					_this.weatherView.weatherLocation.element.find("[value='map']").attr("data-lat", v.lat);
					_this.weatherView.weatherLocation.element.find("[value='map']").attr("data-lon", v.lon);
					$(".change-weather-map-loc").removeClass("hidden");
				}
			}
		}
		else {

			_this.weatherView.weatherLocation.element.find("[value='map']").prop('selected', true);
			_this.weatherView.weatherLocation.element.find("[value='map']").html("Sijainti kartalta");
			_this.weatherView.weatherLocation.element.find("[value='map']").attr("data-lat", v.lat);
			_this.weatherView.weatherLocation.element.find("[value='map']").attr("data-lon", v.lon);
			$(".change-weather-map-loc").removeClass("hidden");
		}
		$(document).trigger("updateWeatherData", {"lat" : v.lat, "lon" : v.lon});
	});

	$(document).on("weatherInfoFromMap", function(e, v) {


		if (typeof v === "undefined") v = {};
		if (!!v.confirm) {

			_this.weatherView.weatherLocation.element.find("[value='map']").attr("data-lat", "");
			_this.weatherView.weatherLocation.element.find("[value='map']").attr("data-lon", "");
			_this.weatherView.weatherLocation.element.find("[value='map']").html("Sijainti kartalta");
		}

		if (!!_this.weatherView.weatherLocation.element.find("[value='map']").attr("data-lat")) {

			var lat = _this.weatherView.weatherLocation.element.find("[value='map']").attr("data-lat");
			var lon = _this.weatherView.weatherLocation.element.find("[value='map']").attr("data-lon");
			return $(document).trigger("updateWeatherData", {"lat" : lat, "lon" : lon});
		}

		$(document).one("mapCoords", function(e, v) {

			_this.weatherView.weatherLocation.element.find("[value='map']").attr("data-lat", v.coords.lat);
			_this.weatherView.weatherLocation.element.find("[value='map']").attr("data-lon", v.coords.lng);
			$(document).trigger("updateWeatherData", {"lat" : v.coords.lat, "lon" : v.coords.lng});
			snapper.open("left");
		});
		snapper.close();
	});

	/*
	 * User view */

	$('#signin-form #persistent').on('click', function(ev) {

		$(ev.currentTarget).toggleClass('checkbox-three-checked');
	});

	$('#toggleEmailNotifications').on('click', function(ev) {

		$(ev.currentTarget).toggleClass('checkbox-three-checked');
		if (_this.profileView.toggleEmailNotifications.element.hasClass('checkbox-three-checked')) {

			$(document).trigger('subscribeEmailNotifications');
		}
		else {

			$(document).trigger('cancelEmailNotifications');
		}
	});


	$('#signin-form').on('submit', function(ev) {

		ev.preventDefault();
		_this.contentNotificationArea.set("");
		_this.signinView.parentElement.find(".signin-email-warning").remove();
		_this.signinView.parentElement.find(".signin-password-warning").remove();

		var fail = false;
		if (_this.signinView.userEmail.get() === "") {

			_this.signinView.userEmail.element.before("<span class='signin-email-warning'>Anna sähköpostiosoitteesi</span>");
			fail = true;
		}
		if (_this.signinView.userPassword.get() === "") {

			_this.signinView.userPassword.element.before("<span class='signin-password-warning'>Salasana ei voi olla tyhjä</span>");
			fail = true;
		}
		if (fail) return false;

		if ($('#signin-form button').attr("data-status") !== "working") {

			$('#signin-form button').attr("data-status", "working");
			var data = {
				'email' : _this.signinView.userEmail.get(),
				'password' : _this.signinView.userPassword.get(),
				'persistent' : $('#signin-form #persistent').hasClass('checkbox-three-checked')
			}
			$(document).trigger('userData', data);
			AppData.signUserIn();
		}
	});

	$('#signout-form').on('submit', function(ev) {

		ev.preventDefault();

		_this.profileView.withdraw();
		_this.releaseView(_this.profileView);
		_this.bindView(_this.signinView, ".view-user-deploy", "div.view-user-signin .close-view")

		_this.userLabel.set('Kirjaudu');

		if (_this.activeView == _this.profileView) {

			_this.activeView = null;
		}

		$(document).trigger('userSignedOut');
	});

	$(document).on('userSignedIn', function(e, v) {

		$('#signin-form button').attr("data-status", "idle");

		_this.signinView.userEmail.set("");
		_this.signinView.userPassword.set("");

		if (_this.signinView.active) {

			_this.signinView.withdraw();
		}
		_this.releaseView(_this.signinView);
		_this.bindView(_this.profileView, ".view-user-deploy", "div.view-user-profile .close-view")

		_this.addCatchView.token.set(AppData.User.token);

		$("#map-ui-toggler").find("li").removeClass("hidden");

		$("[data-signedin='true']").removeClass('hidden');
		$("[data-signedin='false']").addClass('hidden');

		_this.notify("Kirjautuminen onnistui.", "success", "sidebar");
	});

	$(document).on('userSignedInError', function(e, v) {

		$('#signin-form button').attr("data-status", "idle");

		_this.signinView.userPassword.set("");

		if (!_this.signinView.active) {

			_this.deployView(_this.signinView);
		}

		_this.notify("Kirjautuminen epäonnistui.", "warning", "content");
	});

	$(document).on('userSignedOut', function(e, v) {

		_this.addCatchView.token.set("");

		_this.profileView.ownPlaces.element.html("");
		_this.profileView.ownCatchlist.element.html("");

		$("#toggle-own-places").addClass("hidden");
		$("#toggle-own-catches").addClass("hidden");

		_this.weatherView.weatherLocation.element.find("[value='map']").attr("data-lat", "");
		_this.weatherView.weatherLocation.element.find("[value='map']").attr("data-lon", "");
		_this.weatherView.weatherLocation.element.find("[value='map']").html("Sijainti kartalta");
		$(".change-weather-map-loc").addClass("hidden");

		$("[data-signedin='true']").addClass('hidden');
		$("[data-signedin='false']").removeClass('hidden');

		_this.notify("Olet kirjautunut ulos.", "success", "sidebar");
	});

	$(document).on('userProfile', function(e, v) {

		if (typeof v.emailNotifications !== "undefined") {

			if (v.emailNotifications === false) {

				_this.profileView.toggleEmailNotifications.element.removeClass('checkbox-three-checked');
			}
		}

		var ownPlaces = $("#own-places tbody");
		ownPlaces.html("");
		for (var i in v.places){

			var place = v.places[i];
			ownPlaces.append(
			'<tr>'+
				'<td class="table-sub-title">'+ place.name +'</td>'+
				'<td>'+
				'<a class="own-place-show" href="#" data-name="'+ place.name +'" data-lat="'+ place.coords[0] +'" data-lon="'+ place.coords[1] +'">'+
				'<i class="fa fa-map-marker"></i>'+
				'</a>'+
				'</td>'+
				'<td>'+
				'<a class="own-place-delete" href="#" data-name="'+ place.name +'" data-lat="'+ place.coords[0] +'" data-lon="'+ place.coords[1] +'">'+
				'<i class="fa fa-trash-o"></i>'+
				'</a>'+
				'</td>'+
			'</tr>'
			);
		}
	});

	$(document).on("addPlace", function(e, v) {

		$("#new-location-info [name='new-location-lat']").val("");
		$("#new-location-info [name='new-location-lon']").val("");
		$("#new-location-info [name='new-location-name']").val("");

		if (_this.activeView !== _this.profileView) {

			if (snapper.state().state !== "left") {
				snapper.open("left");
			}
			_this.deployView(_this.profileView);
		}

		if (!!v.name && v.name !== "undefined" && v.name !== "Oma sijainti") {

			$("#new-location-info [name='new-location-name']").val(v.name);
		}
		$("#new-location-info [name='new-location-lat']").val(v.lat);
		$("#new-location-info [name='new-location-lon']").val(v.lon);

		$("#new-location-info").removeClass("hidden");
	});

	$("#new-location-info button[value='cancel']").on("click", function(ev) {

		$("#new-location-info [name='new-location-name']").val("");
		$("#new-location-info [name='new-location-lat']").val("");
		$("#new-location-info [name='new-location-lon']").val("");
		$("#new-location-info").addClass("hidden");
	});

	$("#new-location-info button[value='save']").on("click", function(ev) {

		ev.preventDefault();
		var name = $("#new-location-info [name='new-location-name']").val();
		var lat = $("#new-location-info [name='new-location-lat']").val();
		var lon = $("#new-location-info [name='new-location-lon']").val();
		if (name === "") {

			_this.notify("Anna paikalle nimi", "warning", "content");
		}
		else {

			$("#new-location-info [name='new-location-name']").val("");
			$("#new-location-info [name='new-location-lat']").val("");
			$("#new-location-info [name='new-location-lon']").val("");
			$("#new-location-info").addClass("hidden");
			$(document).trigger("savePlace", {name : name, lat : lat, lon : lon});
		}
	});

	$("#own-places").on("click", "a.own-place-show", function(ev) {

		var place = $(ev.currentTarget);
		var data = {"name" : place.attr("data-name"), "lat" : place.attr("data-lat"), "lon" : place.attr("data-lon")};
		$(document).trigger("setPlaceMarker", data);
		if (snapper.state().state === "left" && !$("body").hasClass("wide")) {
			snapper.close();
		}
	});

	$("#own-places").on("click", "a.own-place-delete", function(ev) {

		var place = $(ev.currentTarget);
		var data = {"name" : place.attr("data-name"), "lat" : place.attr("data-lat"), "lon" : place.attr("data-lon")};
		$(document).trigger("deletePlace", data);
	});

	$(document).on('userCatchlist', function(e, v) {

		var ownCatchlist = $("#own-catchlist tbody");
		ownCatchlist.html("");
		for (i in v.catchlist) {

			if (!!v.catchlist[i].Species) {

				var weight = "";
				if (!!v.catchlist[i].Weight) {
					weight = v.catchlist[i].Weight + "kg";
				}
				var marker = "";
				if (!!v.catchlist[i].Lat && !!v.catchlist[i].Lon) {
					marker =	'<a class="own-catch-show" href="#" data-id="'+ v.catchlist[i].ID +'">'+
								'<i class="fa fa-map-marker"></i>'+
								'</a>';
				}
				ownCatchlist.append(
				'<tr>'+
					'<td class="table-sub-title">'+ v.catchlist[i].Species +' '+ weight +'</td>'+
					'<td>'+
					marker+
					'</td>'+
				'</tr>'
				);
			}
		}
	});

	$("#own-catchlist").on("click", "a.own-catch-show", function(ev) {

		var ownCatch = $(ev.currentTarget);
		var data = {"id" : ownCatch.attr("data-id"), "ownCatch" : true};
		$(document).trigger("setCatchMarker", data);
		if (snapper.state().state === "left" && !$("body").hasClass("wide")) {
			snapper.close();
		}
	});

	/*
	 * Check for dimensions */

	var adaptToScreenSize = function() {

		$('body').removeClass('wide');
		$('body').removeClass('narrow');
		$('body').removeClass('small');
		$('body').removeClass('normal');

		$(".all-elements").css({"width" : "100%", "width" : "100vw"});
		if ($(window).width() >= 1080) {

			$('body').addClass('wide');
		}
		else if ($(window).width() >= 720) {

			$('body').addClass('normal');
		}
		else if ($(window).width() >= 350) {
		
			$('body').addClass('small');
		}
		else {

			$('body').addClass('narrow');	
		}

		if (($("body").hasClass("wide") || $("body").hasClass("normal")) && $("#sidebar").attr("data-state") === "open") {

			$(".all-elements").css("width", $(window).width() - 348);
		}
	}
	$(window).resize(adaptToScreenSize);
	adaptToScreenSize();

	/* Detect if iOS WebApp Engaged and permit navigation without deploying Safari */
	(function(a,b,c){if(c in b&&b[c]){var d,e=a.location,f=/^(a|html)$/i;a.addEventListener("click",function(a){d=a.target;while(!f.test(d.nodeName))d=d.parentNode;"href"in d&&(d.href.indexOf("http")||~d.href.indexOf(e.host))&&(a.preventDefault(),e.href=d.href)},!1)}})(document,window.navigator,"standalone")
}

/* Check whether a property or a name of an own property is an TestView object */
KalakeliApp.prototype.isView = function(propertyName)
{
	if (typeof propertyName === "string")
		return (this[propertyName] !== null && typeof this[propertyName].reference !== "undefined" && this[propertyName].reference == "view");
	
	if (typeof propertyName === "object")
		return (this[propertyName] !== null && typeof propertyName.reference !== "undefined" && propertyName.reference == "view");

	return false;
}

/* Bind view to a navigation handler (a selected element's click event) */
KalakeliApp.prototype.bindView = function(view, deployElementSelector, withdrawElementSelector)
{
	var _this = this;

	if (this.isView(view) !== true)
		return false;

	if (typeof view === "string")
		view = this[view];

	view.deployElement = $(deployElementSelector);

	$(deployElementSelector).on('click', function(){
		if (view.parentElement.hasClass("active-view") !== true){
			_this.deployView(view);
		}
	});

	if (!!withdrawElementSelector) {

		view.withdrawElement = $(withdrawElementSelector);

		$(withdrawElementSelector).on('click', function(){
			if (_this.activeView === view) {
				_this.activeView = null;
			}
			view.withdraw(_this.settings.transitionTime);
		});
	}

	return true;
}

KalakeliApp.prototype.releaseView = function(view)
{
	var _this = this;

	if (this.isView(view) !== true)
		return false;

	if (typeof view === "string")
		view = this[view];

	if (!!view.deployElement) {

		view.deployElement.off('click');
	}

	if (!!view.withdrawElement) {

		view.withdrawElement.off('click');
	}

	if (_this.activeView === view) {

		_this.activeView = null;
	}

	view.parentElement.css("display", "none");

	view.deployElement = null;
	view.withdrawElement = null;

	return true;
}

/* Deploy the view specified and withdraw all other known views */
KalakeliApp.prototype.deployView = function(view)
{
	if (this.isView(view) !== true)
		return false;

	if (typeof view === "string")
		view = this[view];

	if (!!this.activeView) {

		this.activeView.withdraw(this.settings.transitionTime);
	}

	if (view.name === "addCatchView") {

		var now = new Date();
		this.addCatchView.time.set(
			("0" + now.getHours()).slice(-2) + ':' + 
			("0" + now.getMinutes()).slice(-2));

		this.addCatchView.date.set(
			("0" + now.getDate()).slice(-2) + '-' + 
			("0" + (now.getMonth() + 1)).slice(-2) + '-' + 
			now.getFullYear());
	}

	if ($("#sidebar").attr("data-state") === "closed") {

		$(document).trigger('openNav');
	}

	view.deploy(this.settings.transitionTime);
	this.activeView = view;

	return true;
}

/* Trigger notification event */
KalakeliApp.prototype.notify = function(message, type, target)
{
	var notificationColors = {
		'success' : 'green',
		'info' : 'blue',
		'warning' : 'yellow',
		'error' : 'red',
	}

	if (!message) return false;
	if (!target) target = "sidebar";
	if (!type) type = "info";

	type = notificationColors[type];
	notification =	'<div class="static-notification-'+ type +' tap-dismiss-notification no-bottom">\n'+
					'\t<p class="center-text uppercase">'+ message +'</p>\n'+
					'</div>\n';

	$(document).trigger(target + "Notification", {'notification' : notification});

	return true;
}

/* View handler for KalakeliApp. Each app view under its parent element and with their functional child elements
are instanciated to AppView objects */
function AppView(viewName, parentElementSelector)
{
	this.reference = "view";
	this.settings = new Object();

	this.parentElementSelector = parentElementSelector;
	this.parentElement = $(parentElementSelector);
	this.deployElement = null;
	this.withdrawElement = null;
	this.active = false;

	this.name = viewName;
}


/* Deploy the view in a nice visual manner */
AppView.prototype.deploy = function(transitionTime)
{
	if (typeof this.settings.defaultTransitionTime === "undefined")
		this.settings.defaultTransitionTime = transitionTime;

	this.parentElement.delay(transitionTime).fadeIn(transitionTime);
	this.parentElement.addClass("active-view");

	if (!!this.deployElement) {

		this.deployElement.addClass("current");
	}

	this.active = true;
}


/* Hide the view in a nice visual manner */
AppView.prototype.withdraw = function(transitionTime)
{
	if (typeof transitionTime === "undefined" && typeof this.settings.defaultTransitionTime !== "undefined")
		transitionTime = this.settings.defaultTransitionTime;
	else
		this.settings.defaultTransitionTime = transitionTime;

	this.parentElement.fadeOut(transitionTime);
	this.parentElement.removeClass("active-view");
	if (!!this.deployElement) {
		this.deployElement.removeClass("current");
	}

	this.active = false;
}

/* Find elements of the view */
AppView.prototype.elements = function()
{
	var elements = new Array();

	for (e in this){
		if (this.hasOwnProperty(e) && typeof this[e].reference !== "undefined" && this[e].reference == "element")
			elements.push(e);
	}

	return elements;
}


/* Add element to view by creating an Element object with the supplied name and selector */
AppView.prototype.add = function(elementName, elementSelector, bindDefault)
{
	this[elementName] = new Element(elementSelector);

	if (typeof bindDefault !== "undefined" && bindDefault === true)
		this.bindDefault(elementName);
}


/* Bind element to custom event trigger to load value (inTrigger) and when value changes (outTrigger) */
AppView.prototype.bind = function(elementName, inTrigger, outTrigger)
{
	if (typeof this[elementName] !== "object" && this[elementName].reference !== "element")
		return false;

	if (typeof inTrigger !== "undefined")
		this[elementName].bindIn(inTrigger);

	if (typeof outTrigger !== "undefined")
		this[elementName].bindOut(outTrigger);
}


/* Bind element to in trigger called <elementName>Load and to out trigger called <elementName>Change */
AppView.prototype.bindDefault = function(elementName)
{
	this.bind(elementName, elementName+"Load", elementName+"Change");
}


function Element(selector)
{
	this.reference = "element"
	this.selector = selector;
	this.element = $(selector);

	this.tagName = this.element.prop("tagName").toLowerCase();
	this.type = "default";

	this.specialType = null;
	this.staticTypes = ["div", "span", "p", "em", "h1", "h2", "h3", "h4", "h5"];
	this.mediaTypes = ["img"];
	this.inputTypes = ["input", "textarea", "select"];
	this.controlTypes = ["button", "a"];

	if ($.inArray(this.tagName, this.staticTypes) > -1)
		this.type = "static";

	if ($.inArray(this.tagName, this.mediaTypes) > -1)
		this.type = "media";

	if ($.inArray(this.tagName, this.inputTypes) > -1)
		this.type = "input";

	if ($.inArray(this.tagName, this.controlTypes) > -1)
		this.type = "control";

	/* Sliders are a special case */
	if (this.selector === "#slider" || this.element.hasClass("slider")){
		this.specialType = "slider";
		this.type = "input";
	}
}

/* Bind element to change it's value on an incoming event trigger */
Element.prototype.bindIn = function(triggerEventName, filter)
{
	var _this = this;
	
	$(document).on(triggerEventName, function(e, v){
		if (typeof filter === "function")
			v = filter(v);
		
		_this.set(v);
	});
}

/* Bind element to broadcast it's value on change */
Element.prototype.bindOut = function(triggerEventName)
{
	var _this = this;

	if (this.specialType === "slider"){
		this.element.on('slide', function(){
			$(document).trigger(triggerEventName, _this.get());
		});

		return true;
	}

	/* Bind input types to onchange event */
	if (this.type === "input"){
		this.element.on('change', function(){
			$(document).trigger(triggerEventName, _this.get());
		});

		return true;
	}

	/* Bind control types to click events */
	if (this.type === "control"){
		this.element.on('click', function(){
			$(document).trigger(triggerEventName, true);
		});

		return true;
	}

	return false;
}

/* Get the element data value if available */
Element.prototype.get = function()
{
	if (this.type === "input")
		return this.element.val();

	if (this.type === "static")
		return this.element.html();

	if (this.type === "media")
		return this.element.attr("src");

	return null;
}

/* Set the element to a value if appropriate */
Element.prototype.set = function(data)
{
	try {
		if (this.type === "input"){
			this.element.val(data);
			return true;
		}

		if (this.type === "static"){
			this.element.html(data);
			return true;
		}

		if (this.type === "media"){
			this.element.attr("src", data);
			return true;
		}

	} catch (e) {
		//console.log("e '"+this.selector+"': " + e);
	}

	return false;
}

var App = new KalakeliApp();

function getParameterByName(name) {
	
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

/**
 * ISO8601 polyfill for Date.toISOString
 *
 *
 */
(function() {
if ( !Date.prototype.toISOString ) {         
    (function() {         
        function pad(number) {
            var r = String(number);
            if ( r.length === 1 ) {
                r = '0' + r;
            }
            return r;
        }      
        Date.prototype.toISOString = function() {
            return this.getUTCFullYear()
                + '-' + pad( this.getUTCMonth() + 1 )
                + '-' + pad( this.getUTCDate() )
                + 'T' + pad( this.getUTCHours() )
                + ':' + pad( this.getUTCMinutes() )
                + ':' + pad( this.getUTCSeconds() )
                + '.' + String( (this.getUTCMilliseconds()/1000).toFixed(3) ).slice( 2, 5 )
                + 'Z';
        };       
    }() );
}
})();

/**
 * Fast UUID generator, RFC4122 version 4 compliant.
 * @author Jeff Ward (jcward.com).
 * @license MIT license
 * @link http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript/21963136#21963136
 **/
var _UUID = (function() {
  var self = {};
  var lut = []; for (var i=0; i<256; i++) { lut[i] = (i<16?'0':'')+(i).toString(16); }
  self.generate = function() {
    var d0 = Math.random()*0xffffffff|0;
    var d1 = Math.random()*0xffffffff|0;
    var d2 = Math.random()*0xffffffff|0;
    var d3 = Math.random()*0xffffffff|0;
    return lut[d0&0xff]+lut[d0>>8&0xff]+lut[d0>>16&0xff]+lut[d0>>24&0xff]+'-'+
      lut[d1&0xff]+lut[d1>>8&0xff]+'-'+lut[d1>>16&0x0f|0x40]+lut[d1>>24&0xff]+'-'+
      lut[d2&0x3f|0x80]+lut[d2>>8&0xff]+'-'+lut[d2>>16&0xff]+lut[d2>>24&0xff]+
      lut[d3&0xff]+lut[d3>>8&0xff]+lut[d3>>16&0xff]+lut[d3>>24&0xff];
  }
  return self;
})();

function onDeviceReady() {

	App.attachListeners();
	App.initViews();

	AppData.attachListeners();
	AppData.init();

	$("[data-signedin='true']").addClass('hidden');
	$("[data-signedin='false']").removeClass('hidden');

	/* Google Analytics
	* Uses a custom implementation
	* to provide PhoneGap support
	*/
	if (typeof window.localStorage['UUID'] == 'undefined') {
		UUID = _UUID.generate();
		window.localStorage.UUID = UUID;
	}
	UUID = window.localStorage['UUID'];
	ga('create', 'UA-51310872-2', {
		'storage': 'none',
		'clientId': UUID,
	});

	if (window.isphone) {

		// make sure that external links start system browser via cordova plugin
		$("a").on('click', function(ev) {

			var link = $(ev.currentTarget);
			var target = link.attr('target');
			if (target === "_blank") {

				ev.preventDefault();
				window.open(link.attr('href'), '_system', 'location=yes');
			}
		});

		ga('set', 'dimension5', 'native');
		$("#storelinks").remove();
	}
	else {

		var catchID = getParameterByName("saalis");
		if (!!catchID) {

			$(document).trigger("setCatchMarker", {"id" : catchID, "ownCatch" : false});
		}

		var noEmail = getParameterByName("sposti");
		if (!!noEmail) {

			$(document).trigger("cancelEmailNotifications");
		}

		ga('set', 'dimension5', 'web');
	}
	ga('send', 'pageview', {'page': 'start'});
}

function showUI() {

	window.clearTimeout(showUITimer);

	window.scrollTo(0,1);

	$("#status").fadeOut();
	$("#preloader").delay(400).fadeOut("slow");
}

(function() {

    // are we running in native app or in a browser?
    window.isphone = false;
    if(document.URL.indexOf("http://") === -1 
        && document.URL.indexOf("https://") === -1) {
        window.isphone = true;
    }

    if( window.isphone ) {

        document.addEventListener("deviceready", onDeviceReady, false);
		window.addEventListener("load", showUI, false);
    }
    else {

    	// in mobile social sharing is handled by a cordova plugin
		window.fbAsyncInit = function() {
			FB.init({
				appId      : '1560349710886312',
				xfbml      : true,
				version    : 'v2.2'
			});
		};

		(function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "js/vendor/fb.sdk.2.2.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

        $(document).ready(onDeviceReady);
		window.addEventListener("load", showUI, false);
    }
})();

// for some reason window.onload does not fire on older(?) Androids, so lets set backup timer to show the UI at some point
var showUITimer = window.setTimeout(showUI, 10000);