var $ = jQuery;

function KalakeliData()
{
	var _this = this;

	// format
	this.User = new Object();
	this.Bins = new Object();
	this.Blackbox = new Object();
	this.Mapbox = new Object();
	this.Weather = new Object();
	this.Cache = new Object();
	this.Compass = new Object();

	// init static
	this.Blackbox = {
		'url' : 'http://otavamedia.vsapis.com/era-xmlrpc/',
		'register' : function(email) {
			return _this.Blackbox.url + '?register&email=' + email;
		},
		'signin' : function(email, password) {
			return _this.Blackbox.url + '?signin&email=' + email + '&password=' + password;
		},
		'listcatch' : function(options) {
			if (typeof _this.User.token === 'undefined') return null;
			if (typeof options !== "object") options = {};
			if (typeof options.updatecache === "undefined") options.updatecache = false;
			return _this.Blackbox.url + '?listcatch&token=' + _this.User.token + (options.updatecache ? '&nottl=1' : '');
		},
		'setprofile' : function(profile) {
			if (typeof _this.User.token === 'undefined') return null;
			if (typeof profile === 'undefined') {
				profile = {'places' : _this.User.places};
				if (typeof _this.User.emailNotifications === "undefined") {

					profile["emailNotifications"] = true;
				}
				else {

					profile["emailNotifications"] = _this.User.emailNotifications;
				}
			} 
			if (typeof profile === 'object') {

				try {

					profile = JSON.stringify(profile);
				}
				catch(err) {

					return null;
				}
			}
			return _this.Blackbox.url + '?setprofile&profile=' + profile + '&token=' + _this.User.token;
		},
		'getprofile' : function() {
			if (typeof _this.User.token === 'undefined') return null;
			return _this.Blackbox.url + '?getprofile&token=' + _this.User.token;
		},
		'geocode' : function(term, limit) {
			if (typeof term === 'undefined') return null;
			if (typeof limit === 'undefined') limit = '';
			return 'http://geographic.vsapis.com/?search&terms=' + escape(term) + '&limit=' + limit;
		},
		'mapbins' : function (level) {
			if (typeof level === 'undefined') level = 0.2;
			return _this.Blackbox.url + '?geojsonbins&mode=normalize&density=' + level;
		},
		'catches' : function (lat0, lon0, lat1, lon1) {
			if (typeof lat0 === 'undefined') lat0 = '';
			if (typeof lon0 === 'undefined') lon0 = '';
			if (typeof lat1 === 'undefined') lat1 = '';
			if (typeof lon1 === 'undefined') lon1 = '';
			return _this.Blackbox.url + '?geojsonall&lat0=' + lat0 + '&lon0=' + lon0 + '&lat1=' + lat1 + '&lon1=' + lon1;
		},
		'probability' : function(lat,lon) {
			if (typeof lat === "undefined" || typeof lon === "undefined") {
				if (typeof _this.User.geoLocation === 'undefined') {
					return false;
				}
				else {
					lat = _this.User.geoLocation.coords.latitude;
					lon = _this.User.geoLocation.coords.longitude;
				}
			}
			return _this.Blackbox.url + '?locationscore&lat='+lat+'&lon='+lon;
		},
		'imageidtourl' : function(id) {
			if (typeof id === "undefined") return false;
			return _this.Blackbox.url + '?imageurl&id=' + id;
		},
		'shareemail' : function(to, url, subject, body, desc, footer) {
			if (typeof to === "undefined" || typeof url === "undefined") return false;
			if (typeof subject === "undefined") subject = encodeURIComponent("Katso tämä saalis Kalakelistä!");
			if (typeof body === "undefined") body = encodeURIComponent("Katso saalis Kalakelista!");
			if (typeof desc === "undefined") desc = encodeURIComponent("Klikkaa tästä");
			if (typeof footer === "undefined") {

				footer = "Terveisin";
				if (!!_this.User.first_name) footer += " " + _this.User.first_name;
				if (!!_this.User.last_name) footer += " " + _this.User.last_name;
				if (footer === "Terveisin") footer += " Kalakeli!";
				footer = encodeURIComponent(footer);
			}
			return _this.Blackbox.url + "?share&to=" + to + "&subject=" + subject + "&body=" + body + "&url=" + url + "&desc=" + desc + "&footer=" + footer;
		},
		'choroplethcoloring' : function(update) {
			return _this.Blackbox.url + "?getchoroplethcoloring" + (typeof update !== "undefined" ? "&updatechoropleth=" : "");
		},
		'updatecache' : function() {
			return _this.Blackbox.url + "?updaterecent=&largechunks=&nottl=1"
		},
		'getcatch' : function(id) {
			if (typeof id === "undefined") return false;
			return _this.Blackbox.url + "?getcatch&id=" + id;
		},
		'getlink' : function(id) {
			if (typeof id === "undefined") return false;
			return _this.Blackbox.url + '?link&id=' + id;
		},
		'feedback' : function(data) {
			if (typeof data !== "object") return false;
			if (!data.message) return false;
			return _this.Blackbox.url + '?feedback&name=' + data.name + '&email=' + data.email + '&message=' + data.message;
		},
	}

	this.Mapbox = {

		"accessToken" : 'pk.eyJ1IjoidmlkYXNvZnQiLCJhIjoiNUN6eE14TSJ9.Wn2rPLRaHJYE2lAwiVs5bQ',
		"mapID" : "vidasoft.i3j9273h",
		"mapContainer" : "map", // #id
		"map" : null,
		"featureLayer" : null,
		"bins" : {},
	}

	this.Weather = {
		'url' : function(lat, lon) {
			if (typeof lat === "undefined" || typeof lon === "undefined") {

				if (typeof this.User.geoLocation === 'undefined') {

					return false;
				}
				else {
					lat = this.User.geoLocation.coords.latitude;
					lon = this.User.geoLocation.coords.longitude;
				}
			}

			return 'http://weather.vsapis.com/?forecast&lat=' + lat + '&lon=' + lon;
		},
		'point' : function(data) {
			if (typeof data !== "object") return false;
			if (typeof data.lat === "undefined" || typeof data.lon === "undefined" || typeof data.date === "undefined") return false;
			return 'http://weather.vsapis.com/?historical&date=' + data.date + '&lat=' + data.lat + '&lon=' + data.lon;
		}
	}

	this.GeoIP = {
		'getLocation' : function() {
			return 'http://geoip.vsapis.com/';
		}
	}

	this.Device = (navigator.userAgent.match(/iPad/i))  == "iPad" ? "iPad" : (navigator.userAgent.match(/iPhone/i))  == "iPhone" ? "iPhone" : (navigator.userAgent.match(/Android/i)) == "Android" ? "Android" : (navigator.userAgent.match(/BlackBerry/i)) == "BlackBerry" ? "BlackBerry" : "null";

}

KalakeliData.prototype.init = function() {

	var _this = this;

	var southWest = L.latLng(55.924, -15.864),
        northEast = L.latLng(73.163, 68.510),
        bounds = L.latLngBounds(southWest, northEast);

	L.mapbox.accessToken = this.Mapbox.accessToken;
	this.Mapbox.map = L.mapbox.map(this.Mapbox.mapContainer, this.Mapbox.mapID, {minZoom: 5, zoomControl: false});
	new L.Control.Zoom({position: 'topright'}).addTo(this.Mapbox.map);

	this.Mapbox.catchesLayer = new L.MarkerClusterGroup({disableClusteringAtZoom:10, chunkedLoading: true, polygonOptions: {
        fillColor: '#00abf0',
        color: '#3887be',
        weight: 2,
        opacity: 1,
        fillOpacity: 0.5
      }});
	this.Mapbox.motonetLayer = {};
	this.Mapbox.provinceLayer = {};
	this.Mapbox.ownPlacesLayer = L.mapbox.featureLayer();
	this.Mapbox.ownCatchesLayer = L.mapbox.featureLayer();

	this.Mapbox.map.on('load', function(e) {

		$.ajax({'url' : 'geodata/motonet.kml'}).done(function(data){
	        _this.Mapbox.motonetLayer = omnivore.kml.parse(data);
	        // Set branded markers
	        _this.Mapbox.motonetLayer.eachLayer(function(marker) {
	            marker.setIcon(
	                    L.icon({
	                          iconUrl: 'images/marker-3.png',
	                          iconSize: [36, 48],
	                          iconAnchor: [18, 48]
	                    })
	            );
	            
	            // Format link to Motonet website
	            marker.feature.properties.slug = marker.feature.properties.name.toLowerCase().replace(new RegExp('ä', 'g'), 'a');
	            var href = 'http://motonet.fi/fi/tavaratalo/' + marker.feature.properties.slug;
	            
	            var tracker_str = "trackMarkerEvent(\'motonet_site_visit\', \'motonet_" + marker.feature.properties.slug + "\', " + marker.getLatLng()['lat'] + "," + marker.getLatLng()['lng'] + ");"
	            
	            // Add phone numbers on mobile devices
	            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	                   marker.feature.properties.description = marker.feature.properties.description
	                    .replace(/(\d\d\d-\d\d\d\d\d\d\d)/g,'<a href="tel:$1"><b>$1</b></a>');
	            }  
	            // Convert line breaks
	            marker.feature.properties.description = marker.feature.properties.description.replace(new RegExp("\n", 'g'), "<br/>");

	            marker.bindPopup(
	                '<div class="simple"><h3>Motonet '
	                +marker.feature.properties.name+'<a class="remove-marker pull-right" onclick="$(document).trigger(\'removeMarker\');"><i class="fa fa-times"></i></a></h3>'
	                +marker.feature.properties.description + '<br><br>' +
	                '<a href="' + href + '" target="_blank" onclick="'+tracker_str+'">' + 'Liikkeen sivut</a>'
	                + '</div>');
	            
	            // Marker interaction metrics
	            marker.on('popupopen', function(){
	                var coord = this
	                trackMarkerEvent('motonet_popup_open',
	                    'motonet_' + this.feature.properties.slug,
	                    this.getLatLng()['lat'],
	                    this.getLatLng()['lng']
	                );   
	            });
	            
	        });

			$(document).on('mapZoom', function(e, v) {

				if (v.target._zoom > 7) {

					if (!_this.Mapbox.map.hasLayer(_this.Mapbox.motonetLayer)) {

						_this.Mapbox.motonetLayer.addTo(_this.Mapbox.map);
					}
				}
				else if (_this.Mapbox.map.hasLayer(_this.Mapbox.motonetLayer)) {

					_this.Mapbox.map.removeLayer(_this.Mapbox.motonetLayer);
				}
			});
		});

		$.ajax('geodata/fi-all.geo.json').done(function(data) {

			if (typeof data !== "object") {

				try {

					data = $.parseJSON(data);
				}
				catch (err) {

					return false;
				}
			}
			
			_this.Mapbox.provinceLayer = L.geoJson(data, {
				style: function(feature) {

					return {
						weight: 2,
						opacity: 0.1,
						color: 'black',
						fillOpacity: 1,
						fillColor: '#C7D4FF'
					};
				}
			}).on("click", function(ev) {

				$(document).trigger("mapCoords", {"coords" : ev.latlng});

				// Doubleclick = zoom, do not place marker
				if (!!_this.Mapbox.clickTimer) {

					window.clearTimeout(_this.Mapbox.clickTimer);
					delete _this.Mapbox.clickTimer;
					_this.Mapbox.map.setZoomAround(ev.latlng, _this.Mapbox.map.getZoom() + 1);
				}
				else {

					_this.Mapbox.clickTimer = window.setTimeout(function() {

						delete _this.Mapbox.clickTimer;
						_this.setMarker(ev);
					}, 200);
				}

			});

			$.ajax({'url' : _this.Blackbox.choroplethcoloring()})
				.done(function(colors) {

					if (typeof colors !== "object") {

						try {

							colors = $.parseJSON(colors);
						}
						catch (err) {

							return false;
						}
					}
					_this.Mapbox.provinceLayer.eachLayer(function(layer) {

						for (var i in colors["features"]) {
							var feature = colors["features"][i];
							if (feature.properties.code === layer.feature.properties.code) {

								layer.setStyle({fillColor : feature.properties.fillColor});
								continue;
							}
						}
					});
			});
		});

		_this.Mapbox.map.setZoom(5);
		_this.Mapbox.lastZoom = 5;
		_this.Mapbox.map.fitBounds(bounds);
		$(document).trigger('mapInit');
	});
	this.Mapbox.map.on('zoomend', function(ev) {

		$(document).trigger('mapZoom', ev);
	});
	this.Mapbox.map.on('zoomstart', function(ev) {

		_this.Mapbox.last_zoom = _this.Mapbox.map.getZoom();
	});
	this.Mapbox.map.on("moveend", function(ev) {

		_this.checkCatches();
	});
	this.Mapbox.map.on("click", function(ev) {

		$(document).trigger("mapCoords", {"coords" : ev.latlng})

		/* Doubleclick = zoom, do not place marker */
		if (!!_this.Mapbox.clickTimer) {

			window.clearTimeout(_this.Mapbox.clickTimer);
			delete _this.Mapbox.clickTimer;
		}
		else {

			_this.Mapbox.clickTimer = window.setTimeout(function() {

				delete _this.Mapbox.clickTimer;
				_this.setMarker(ev);
			}, 200);
		}
	});
	this.Mapbox.map.on("popupopen", function(ev) {

		_this.getLocationScore({lat : ev.popup._latlng.lat, lon : ev.popup._latlng.lng});
		var popup = $(ev.popup._container);
		if (popup.find(".catch-popup").length) {

			$("body").addClass("catchpopup");

			if (_this.Mapbox.popup !== ev.popup) {

				var extPopup = L.popup()
								.setLatLng(ev.popup._latlng)
								.setContent(ev.popup._content);

				_this.Mapbox.popup = extPopup;
				extPopup.openOn(_this.Mapbox.map);

				if (popup.find(".catch-image").length) {

					if (!!popup.find(".catch-image").attr("data-id")) {

						$.ajax(_this.Blackbox.imageidtourl(popup.find(".catch-image").attr("data-id"))).done(function(response) {

							if (typeof response !== "object") {

								try {

									response = $.parseJSON(response);
								}
								catch (err) {

									popup.find(".catch-image").html("").removeAttr("data-id");
									return false;
								}
							}

							var img = new Image();
							img.onload = function() {

								popup.find(".catch-image").html("").removeAttr("data-id").append(img);
								ev.popup._source.bindPopup(popup.find(".leaflet-popup-content").html());
								extPopup.setContent(popup.find(".leaflet-popup-content").html());								
							}
							img.src = response.link;
						});
					}
				}
				if (typeof popup.find("a.story-link").attr("onclick") === "undefined") {

					$.ajax(_this.Blackbox.getlink(popup.find(".catch-popup").attr("data-id"))).done(function(data) {

						if (typeof data !== "object") {

							try {

								data = $.parseJSON(data);
							}
							catch (err) {

								return false;
							}
						}
						if (window.isphone) {

							popup.find(".story-link").attr("onclick", "window.open('"+data.link+"', '_system', 'location=yes')");
						}
						else {

							popup.find(".story-link").attr("onclick", "window.open('"+data.link+"')");	
						}
						popup.find(".story-link").attr("data-status", "idle");
						popup.find(".story-link").removeAttr("disabled");
						popup.find(".story-link i").remove();
						ev.popup._source.bindPopup(popup.find(".leaflet-popup-content").html());
						extPopup.setContent(popup.find(".leaflet-popup-content").html());
					});
				}

				if (!popup.find(".degrees").length) {

					var d = popup.find(".date").html();
					var dateParts = d.split(".");
					if (dateParts.length < 3) {

						popup.find(".weather.value").append('<span class="degrees"></span>');
						ev.popup._source.bindPopup(popup.find(".leaflet-popup-content").html());
						extPopup.setContent(popup.find(".leaflet-popup-content").html());
					}
					var date;
					if (dateParts[2].length === 4) {

						date = dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];
					}
					else {

						date = dateParts[0] + "-" + dateParts[1] + "-" + dateParts[2];	
					}
					$.ajax(_this.Weather.point({lat : ev.popup._latlng.lat, lon : ev.popup._latlng.lng, date : date}))
						.done(function(data) {

							if (typeof data !== "object") {

								try {

									data = $.parseJSON(data);
								}
								catch (err) {

									popup.find(".weather.value").append('<span class="degrees"></span>');
									ev.popup._source.bindPopup(popup.find(".leaflet-popup-content").html());
									extPopup.setContent(popup.find(".leaflet-popup-content").html());
								}
							}
							var temp = "";
							var weatherIcon = "";
							var rain = 0;
							var snow = 0;
							for (var i in data) {

								var d = data[i];
								if (d.id === "tday") {

									temp = d.data[0].value + "&deg;c";
								}
								else if (d.id === "rrday") {

									rain = parseInt(d.data[0].value);
								}
								else if (d.id === "snow") {

									snow = parseInt(d.data[0].value);
								}
							}
							if (rain > snow && rain > 0) {

								weatherIcon = '<img src="images/weather/4.png" />';
								popup.find(".weather.value img").remove();
							}
							else if (snow > rain && snow > 0) {

								weatherIcon = '<img src="images/weather/5.png" />';
								popup.find(".weather.value img").remove();
							}
							else if (rain === snow && rain > 0) {

								weatherIcon = '<img src="images/weather/4.png" />';
								popup.find(".weather.value img").remove();
							}
							popup.find(".weather.value").prepend('<span class="degrees">' + temp + '</span>' + weatherIcon);
							ev.popup._source.bindPopup(popup.find(".leaflet-popup-content").html());
							extPopup.setContent(popup.find(".leaflet-popup-content").html());
						});
				}
			}
		}
		else {

			_this.Mapbox.popup = ev.popup;
		}
	});

	this.getSessionCookie();
	this.getUserLocation();
}

KalakeliData.prototype.attachListeners = function()
{

	var _this = this;

	Compass.noSupport(function () {

		$("#toggle-compass").remove();
		$("#compass").remove();
	});

	Compass.needGPS(function () {
		// request for GPS
	}).needMove(function () {
		// earching for signal
	}).init(function () {
		$("#toggle-compass").removeClass("hidden");
	});

	$("#toggle-compass").on("click", function(ev) {

		ev.stopImmediatePropagation();
		ev.preventDefault();
		var toggle = $(ev.currentTarget);
		if (toggle.hasClass("active")) {

			$("#compass").addClass("hidden");
			if (!!_this.Compass.id) {

				Compass.unwatch(_this.Compass.id);
			}
		}
		else {

			$("#compass").removeClass("hidden");
			_this.Compass.id = Compass.watch(function (heading) {

				_this.updateCompass(heading);
			});
		}
		toggle.toggleClass("active");
	});

	$("#toggle-catch").on("click", function(ev) {

		ev.stopImmediatePropagation();
		ev.preventDefault();
		var toggle = $(ev.currentTarget);
		if (toggle.hasClass("active")) {

			_this.Mapbox.map.removeLayer(_this.Mapbox.catchesLayer);
		}
		else {

			_this.Mapbox.catchesLayer.addTo(_this.Mapbox.map);
		}
		toggle.toggleClass("active");
	});

	$("#toggle-probability").off('click').on("click", function(ev) {

		ev.stopImmediatePropagation();
		ev.preventDefault();
		var toggle = $(ev.currentTarget);
		if (toggle.hasClass("active")) {

			_this.Mapbox.map.removeLayer(_this.Mapbox.provinceLayer);
		}
		else {

			_this.Mapbox.provinceLayer.addTo(_this.Mapbox.map);
		}
		toggle.toggleClass("active");
	});

	$("#toggle-current-location").on("click", function(ev) {

		ev.stopImmediatePropagation();
		ev.preventDefault();
		var toggle = $(ev.currentTarget);

		if (!!_this.User.geoLocation) {

			if (!!_this.User.geoLocation.coords) {

				return _this.setMarker({"name" : "Oma sijainti", "lat" : _this.User.geoLocation.coords.latitude, "lon" : _this.User.geoLocation.coords.longitude, "own" : true});
			}
		}

		$(document).one("userGeoLocation", function() {

			_this.setMarker({"name" : "Oma sijainti", "lat" : _this.User.geoLocation.coords.latitude, "lon" : _this.User.geoLocation.coords.longitude, "own" : true});
		});
		_this.getUserLocation();
	});

	$("#toggle-own-places").on("click", function(ev) {

		ev.stopImmediatePropagation();
		ev.preventDefault();
		var toggle = $(ev.currentTarget);
		if (toggle.hasClass("active")) {

			_this.Mapbox.map.removeLayer(_this.Mapbox.ownPlacesLayer);
		}
		else {

			_this.Mapbox.ownPlacesLayer.addTo(_this.Mapbox.map);
		}
		toggle.toggleClass("active");
	});

	$("#toggle-own-catches").on("click", function(ev) {

		ev.stopImmediatePropagation();
		ev.preventDefault();
		var toggle = $(ev.currentTarget);
		if (toggle.hasClass("active")) {

			_this.Mapbox.map.removeLayer(_this.Mapbox.ownCatchesLayer);
		}
		else {

			_this.Mapbox.ownCatchesLayer.addTo(_this.Mapbox.map);
		}
		toggle.toggleClass("active");
	});

	/*
	 * USER
	 **/

	$(document).on("registrationSuccess", function(e, v) {

		_this.signUserIn();
	});

	$(document).on('userData', function(e, v){

		_this.User.first_name = v.first_name || _this.User.first_name || null;
		_this.User.last_name = v.last_name || _this.User.last_name || null;

		_this.User.persistent = v.persistent || _this.User.persistent || false;

		_this.User.email = v.email || _this.auth.email || null;
		_this.User.password = v.password || _this.User.password || null;
		_this.User.token = v.token || _this.User.token || null;
		_this.User.user_id = v.user_id || _this.User.user_id || null;
	});

	$(document).on('userSignedIn', function(e, v) {

		_this.getUserProfile();
		_this.getUserCatchlist();
	});

	$(document).on('userCatchlist', function(e ,v) {

		for (var i in v.catchlist) {

			var c = v.catchlist[i];
			if (!c.Lat || !c.Lon) continue;
			var data = {
				"date" : c.DateAdded,
				"catcher" : c.Author,
				"species" : c.Species,
				"lat" : c.Lat,
				"lon" : c.Lon,
				"img" : c.ImageURL,
				"weight" : c.Weight,
				"rain" : c.RainMillimeters,
				"snow" : c.SnowMillimeters,
				"temp" : c.TempAvg,
				"id" : c.ID,
				"ownCatch" : true
			};
			var marker = _this.createCatchMarker(data);
			_this.Mapbox.ownCatchesLayer.addLayer(marker);
		}
	});

	$(document).on('userProfile', function(e ,v) {

		for (var i in v.places) {

			var p = v.places[i];
			var data = {
				"name" : p.name,
				"lat" : p.coords[0],
				"lon" : p.coords[1],
				"ownPlace" : true,
			};
			var marker = _this.createPlaceMarker(data);
			_this.Mapbox.ownPlacesLayer.addLayer(marker);
		}
	});

	$(document).on('userSignedOut', function(e, v) {

		_this.User = new Object();
		_this.Mapbox.ownPlacesLayer = L.mapbox.featureLayer();
		_this.Mapbox.ownCatchesLayer = L.mapbox.featureLayer();
		$(document).trigger("removeMarker");
		var cookie_string = "kalakeli_session=; path=/; expires=Thu, 01 Jan 1970 00:00:00 UTC";
		document.cookie = cookie_string;
		$(document).trigger("userProfile", _this.User);
	});

	$(document).on("registrationNeeded", function() {

		_this.register();
	});

	$(document).on("userGeoLocation", function() {

		_this.getWeatherData({"lat" : _this.User.geoLocation.coords.latitude, "lon" : _this.User.geoLocation.coords.longitude});
		_this.getLocationScore({"lat" : _this.User.geoLocation.coords.latitude, "lon" : _this.User.geoLocation.coords.longitude});
	});

	$(document).on("cancelEmailNotifications", function() {

		if (!!_this.User.token) {

			_this.User.emailNotifications = false;
			_this.setUserProfile();
		}
		else {

			$(document).one("userProfile", function() {

				_this.User.emailNotifications = false;
				_this.setUserProfile();
			});
			$(document).trigger("needSignIn");
		}
	});

	$(document).on("subscribeEmailNotifications", function() {

		if (!!_this.User.token) {

			_this.User.emailNotifications = true;
			_this.setUserProfile();
		}
		else {

			$(document).one("userProfile", function() {

				_this.User.emailNotifications = true;
				_this.setUserProfile();
			});
			$(document).trigger("needSignIn");
		}
	});

	/*
	 * CATCH
	 **/

	$(document).on("updateCache", function(e, v) {

		$.ajax(_this.Blackbox.updatecache()).done(function(response) {

			$(document).trigger("cacheUpdated");
		});
	});

	$(document).on('updateOwnCatches', function(e, v) {

		_this.getUserCatchlist({"updatecache" : true});
	});

	$(document).on("catches", function(e, v) {

		var catches = [];
		for (var i = 0; i < v.features.length; i++) {
	        var c = v.features[i];

	        var title_parts = c.properties.title.split(" (");
	        var catcher_info = title_parts[0];
	        var catch_info_parts = title_parts[1].substr(0, title_parts[1].length - 1).split(" ");
	        var catch_species = "";
	        for (var j = 0; j < catch_info_parts.length - 2; j++) {

	        	catch_species += catch_info_parts[j] + " ";
	        }
	        var catch_weight = catch_info_parts[catch_info_parts.length - 2];
			var data = {
				"id" : c.properties.id,
				"date" : c.properties.published,
				"catcher" : catcher_info,
				"species" : catch_species,
				"lat" : c.geometry.coordinates[1],
				"lon" : c.geometry.coordinates[0],
				"img" : c.properties.image,
				"weight" : catch_weight,
				"rain" : c.properties.rrday,
				"snow" : c.properties.snow,
				"temp" : c.properties.tday
			};
			var c = _this.createCatchMarker(data);
			catches.push(c);
	    }
	    _this.Mapbox.catchesLayer.addLayers(catches);
	    _this.Mapbox.map.addLayer(_this.Mapbox.catchesLayer);
	});

	$(document).on("setCatchMarker", function(e, v) {

		if (v.ownCatch) {

			var c = null;
			for (var i in _this.User.catchlist) {

				if (""+_this.User.catchlist[i].ID === ""+v.id) {
					c = _this.User.catchlist[i];
					break;
				}
			}

			var data = {
				"date" : c.DateAdded,
				"catcher" : c.Author,
				"species" : c.Species,
				"lat" : c.Lat,
				"lon" : c.Lon,
				"img" : c.ImageURL,
				"weight" : c.Weight,
				"rain" : c.RainMillimeters,
				"snow" : c.SnowMillimeters,
				"temp" : c.TempAvg,
				"id" : c.ID,
				"ownCatch" : true
			};
			_this.setCatchMarker(data);
		}
		else {

			$.ajax(_this.Blackbox.getcatch(v.id))
				.done(function(response) {

					var c = response;
					if (typeof c !== "object") {

						try {

							c = $.parseJSON(c);
						}
						catch (err) {

							return false;
						}
					}
					var data = {
						"date" : c[0].DateAdded,
						"catcher" : c[0].Author,
						"species" : c[0].Species,
						"lat" : c[0].Lat,
						"lon" : c[0].Lon,
						"img" : c[0].ImageURL,
						"weight" : c[0].Weight,
						"rain" : c[0].RainMillimeters,
						"snow" : c[0].SnowMillimeters,
						"temp" : c[0].TempAvg,
						"id" : c[0].ID,
						"ownCatch" : false
					};
					_this.setCatchMarker(data);
			});
		}
	});


	/*****************
	 *
	 * LOCATION
	 **/

	$(document).on("setPlaceMarker", function(e, v) {

		if (typeof v !== "object") {

			try {
				v = $.parseJSON(v);
			}
			catch(err) {
				return false;
			}
		}
		if (!v.lat || !v.lon) return false;

		if (!!v.name && v.type !== "location-search") {

			v.ownPlace = true;
		}
		_this.setMarker(v);
	});

	$(document).on("savePlace", function(e, v) {

		if (typeof v.name === "undefined") {

			return false;
		}
		if (v.name === "") {

			return false;
		}

		_this.User.places.push({'coords' : [v.lat, v.lon], 'name' : v.name});
		_this.setUserProfile();
		v.ownPlace = true;
		_this.setMarker(v)
		$(document).trigger("userProfile", _this.User);
		$(document).trigger("locationAdded");
	});

	$(document).on("deletePlace", function(e, v) {

		for (var i in _this.User.places) {

			var place = _this.User.places[i];
			if (place.name === v.name) {

				delete _this.User.places[i];
				_this.setUserProfile();
				$(document).trigger("userProfile", _this.User);
				$(document).trigger("locationDeleted");
			}
		}

		if (!!_this.Mapbox.popup) {

			var popup = $(_this.Mapbox.popup._content);
			if (v.name === popup.attr("data-title")) {

				$(document).trigger('removeMarker');
			}
		}
	});

	$(document).on("searchLocation", function(e, v) {

		if (typeof v === "undefined") return false;
		if (!!v.place) {
			_this.searchLocation(v);
		}
		else {
			return false;
		}
	});


	/*****************
	 *
	 * MARKERS
	 **/

	$(document).on("removeMarker", function(e, v) {

		e.stopImmediatePropagation();
		if (!!_this.Mapbox.marker) {

			_this.Mapbox.map.removeLayer(_this.Mapbox.marker);
			delete _this.Mapbox.marker;
		}
		$(document).trigger("closePopup", v);
	});

	$(document).on("closePopup", function(e, v) {

		_this.Mapbox.map.closePopup();
		if (typeof v === "object") {

			if (typeof v.type !== "undefined" && v.type === "catch-marker") {

				$("body").removeClass("catchpopup");
			}
		}
	});

	$(document).on("markerShare", function(e, v) {

		if (!!_this.Mapbox.popup) {

			var p = $(_this.Mapbox.popup._container);

			if (v.to === "email" && p.find(".share-email-address").hasClass("hidden")) {

				p.find(".share-email-address").removeClass("hidden");
				_this.Mapbox.map.panBy([0, -50]);
				return true;
			}

			var data = {};
			data["id"] = p.find(".catch-popup").attr("data-id");
			data["description"] = "";
			if (!!p.find(".catcher").length && p.find(".catcher").html()) {

				data["description"] += p.find(".catcher").html() + " - ";
			}
			if (!!p.find(".catch-popup").attr("data-title")) {

				data["description"] += p.find(".catch-popup").attr("data-title");	
			}
			data["picture"] = "";
			if (p.find(".catch-image").length) {

				if (p.find(".catch-image img").length) {

					data["picture"] = p.find(".catch-image img").attr("src");
					return v.to === "fb" ? $(document).trigger("fbShare", data) :
							v.to === "twitter" ? $(document).trigger("twitterShare", data) :
							v.to === "email" ? $(document).trigger("emailShare", data) : false;

				}
				else if (!!p.find(".catch-image").attr("data-id")) {

					$.ajax(_this.Blackbox.imageidtourl(p.find(".catch-image").attr("data-id"))).done(function(response) {

						if (typeof response !== "object") {

							try {

								response = $.parseJSON(response);
								data.picture = response.link;
							}
							catch (err) {

								//...
							}
						}
						return v.to === "fb" ? $(document).trigger("fbShare", data) :
								v.to === "twitter" ? $(document).trigger("twitterShare", data) :
								v.to === "email" ? $(document).trigger("emailShare", data) : false;
					});
				}
			}
			else {

				return v.to === "fb" ? $(document).trigger("fbShare", data) :
						v.to === "twitter" ? $(document).trigger("twitterShare", data) :
						v.to === "email" ? $(document).trigger("emailShare", data) : false;
			}
		}
	});


	/*****************
	 *
	 * SOME
	 **/

	$(document).on("fbShare", function(e, v) {

		var url = 'http://kalakeli.fi/?saalis=' + v.id;
        if (typeof window.plugins !== "undefined") {

            if (typeof window.plugins.socialsharing !== "undefined") {

            	if (_this.Device === "Android") {

            		// Android does not support for sharing both an image AND a link
            		// nor does it support prefilled text at all
            		// the link seems more important
            		window.plugins.socialsharing.shareViaFacebook(v.description, null, url);	
            	}
            	else {

                	window.plugins.socialsharing.shareViaFacebook(v.description, v.picture, url);
                }
                return true;
            }
        }
		FB.ui({
			method: 'feed',
			link: url,
			caption: 'kalakeli.fi - kalastajan kaveri',
			description : v.description,
			picture : v.picture
		}, function(response){});
	});

	$(document).on("twitterShare", function(e, v) {

		var url = 'http://kalakeli.fi/?saalis=' + v.id;
        if (typeof window.plugins !== "undefined") {

            if (typeof window.plugins.socialsharing !== "undefined") {

                window.plugins.socialsharing.shareViaTwitter(v.description, v.picture, url)
                return true;
            }
        }
		window.open('http://twitter.com/share?url=' + encodeURIComponent(url) +
				'&text=' + encodeURIComponent(v.description) + '&', 'twitterwindow',
				'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +
				',toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
	});

	$(document).on("emailShare", function(e, v) {

		var p = $(_this.Mapbox.popup._container);
		p.find(".share-email-address i").addClass("fa-spin");
		p.find("[name='share-email-address']").addClass("hidden");

		var to = p.find("[name='share-email-address']").val();
		var url = 'http://kalakeli.fi/?saalis=' + v.id;
		$.ajax(_this.Blackbox.shareemail(to, encodeURIComponent(url))).done(function(response) {

			p.find(".share-email-address i").removeClass("fa-spin");
			p.find(".share-email-address").prepend('<div class="static-notification-green tap-dismiss-notification no-bottom"><p class="center-text uppercase">Sähköposti lähetetty!</p></div>');
			p.find("[name='share-email-address']").val("");
		});
	});


	/***************************
	 *
	 * MISC
	 **/

	$(document).on("updateWeatherData", function(e, v) {

		if (typeof v === "undefined") v = {};
		if (typeof v.lat !== "undefined") {

			_this.getWeatherData(v);
		}
		else {

			if (typeof _this.User.geoLocation === "object") {
				if (typeof _this.User.geoLocation.coords === "object") {
					v = {"lat" : _this.User.geoLocation.coords.latitude, "lon" : _this.User.geoLocation.coords.longitude};
					return _this.getWeatherData(v);
				}
			}
			_this.getUserLocation();
		}
	});

	$(document).on('mapInit', function(e, v) {

		_this.getMapFeatures();
	});

	$(document).on("needLocationProbability", function(e, v) {

		_this.getLocationScore(v);
	});

	$(document).on("userLocationNeeded", function(e, v) {

		_this.getUserLocation();
	});

	$(document).on("sendFeedback", function(e, v) {

		if (typeof v !== "object") {
		
			return $(document).trigger("feedbackError");
		}
		if (typeof v.message === "undefined" || !v.message) {

			return $(document).trigger("feedbackError", {"message" : "Viesti ei voi olla tyhjä."});
		}
		if (typeof v.email === "undefined") v.email = "";
		if (typeof v.name === "undefined") v.name = "";
		$.ajax(_this.Blackbox.feedback(v))
			.done(function(response) {

				$(document).trigger("feedbackSuccess");
			});
	});
}

KalakeliData.prototype.getLocationScore = function(data)
{
	var _this = this;

	if (typeof data !== "object") data = {};

	$.ajax(this.Blackbox.probability(data.lat, data.lon))
		.done(function(response) {

			if (typeof response !== "object") {

				try {

					response = $.parseJSON(response);
				}
				catch (err) {

					response = {'score' : Math.random() * 50 + 49};
				}
			}

			if (!!response.score) {

				$(document).trigger("locationProbability", response);
			}
			else {

				response.score = Math.random() * 50 + 49;
				$(document).trigger("locationProbability", response);	
			}
		});
}

KalakeliData.prototype.setMarker = function(data)
{
	if (typeof data.latlng === "undefined" && typeof data.lat === "undefined") return false;

	data.lat = data.lat || data.latlng.lat;
	data.lon = data.lon || data.latlng.lng;

	var marker = null;
	if (marker = this.createPlaceMarker(data)) {

		this.getLocationScore({lat : data.lat, lon : data.lon});

		if (!!this.Mapbox.marker) {

			this.Mapbox.map.removeLayer(this.Mapbox.marker);
			delete this.Mapbox.marker;
		}
		if (!!this.Mapbox.popup) {

			this.Mapbox.map.closePopup();
			delete this.Mapbox.popup;
		}
		marker.addTo(this.Mapbox.map);
		if (data.name == "Oma sijainti") {

			this.Mapbox.map.setView([data.lat, data.lon], 12);
		}
		else {

			this.Mapbox.map.panTo([data.lat, data.lon]);
			marker.openPopup();
		}

		this.Mapbox.marker = marker;
	}
}

KalakeliData.prototype.createPlaceMarker = function(data)
{

	var title = "Uusi paikka";
	if (!!data.name && data.name !== "undefined" && data.name !== "") {

		title = data.name;
	}

	var color = 'ecf0f1';
	if (!!data.ownPlace) {

		color = 'a5d27c';
	}
	else if (!!data.own) {

		color = 'a57cd2';
	}

	var marker = L.marker([data.lat, data.lon], {
	            title: title,
	            icon: L.mapbox.marker.icon({'marker-symbol' : 'circle-stroked', 'marker-color': color})
	        });

	var popup = '<div data-title="' + title + '" class="marker-popup"><h2>'+ title +'<a class="remove-marker pull-right" onclick="$(document).trigger(\'removeMarker\');"><i class="fa fa-2x fa-times"></i></a></h2>';
	    popup += '<div class="popup-buttons">';
	    popup += '<button onclick="$(document).trigger(\'showWeather\', {name : \''+ data.name +'\', lat : \'' + data.lat + '\',  lon : \'' + data.lon + '\'})" class="button button-blue button-wide">Katso sää</button>';
	    popup += '<button onclick="$(document).trigger(\'addCatchToLocation\', {name : \''+ data.name +'\', lat : \'' + data.lat + '\',  lon : \'' + data.lon + '\'})" class="button button-light button-wide">Lisää saalis</button>';

	if (!!data.ownPlace) {

	    popup += '<div class="decoration"></div>';
		popup += '<button onclick="$(document).trigger(\'deletePlace\', {name : \''+ data.name +'\', lat : \'' + data.lat + '\',  lon : \'' + data.lon + '\'})" class="remove-location-button button button-red button-wide">Poista omista paikoista</button>';
	}
	else if (!!this.User.token) {

		var name = data.name || '';
	    popup += '<div class="decoration"></div>';
	    popup += '<button onclick="$(document).trigger(\'addPlace\', {lat : \'' + data.lat + '\',  lon : \'' + data.lon + '\', name : \'' + data.name + '\'})" class="add-location-button button button-blue button-wide">Tallenna omiin paikkoihin</button>';
	}
    popup += '</div>';
	marker.bindPopup(popup);
	return marker;
}

KalakeliData.prototype.setCatchMarker = function(data)
{

	var marker = this.createCatchMarker(data);
	if (!!this.Mapbox.marker) {

		this.Mapbox.map.removeLayer(this.Mapbox.marker);
		delete this.Mapbox.marker;
	}
	if (!!this.Mapbox.popup) {

		this.Mapbox.map.closePopup();
		delete this.Mapbox.popup;
	}

	this.getLocationScore({lat : data.lat, lon : data.lon});
	marker.addTo(this.Mapbox.map);
	this.Mapbox.map.panTo([data.lat, data.lon]);
	this.Mapbox.marker = marker;
	marker.openPopup();
}

KalakeliData.prototype.createCatchMarker = function(data)
{

	var catcher = !!data.catcher ? data.catcher : "-";
	var weight = "";
	if (!!data.weight) {

		if (data.weight !== "0.0") {

			weight = data.weight + "kg";
		}
	}
	var species = "Saalis";
	if (data.species.indexOf("Muu") === -1) {

		species = data.species;
	}
	var date_parts = data.date.split("-");
	var date = "-";
	if (date_parts.length === 3) {

		if (date_parts[0].length === 4) {

			date = date_parts[2] +'.'+ date_parts[1] +'.'+ date_parts[0];
		}
		else {

			date = date_parts[0] +'.'+ date_parts[1] +'.'+ date_parts[2];	
		}
	}
	var rain = !!data.rain ? data.rain : 0;
	var snow = !!data.snow ? data.snow : 0;
	var temp = "";
	if (typeof data.temp !== "undefined" && data.temp !== "" && data.temp !== null && data.temp !== "-" && data.temp !== "undefined") {
	
		temp = '<span class="degrees">' + data.temp + '&deg;c</span>';
	}

	var color = '00abf0';
	if (!!data.ownCatch) {

		color = 'fd9c73';
	}

	var marker = L.marker([data.lat, data.lon], {
	            title: species + ' ' + weight,
	            icon: L.mapbox.marker.icon({'marker-symbol' : 'circle-stroked', 'marker-color': color})
	        });

	var popup = '<div class="catch-popup" data-id="'+data.id+'" data-title="' + species + ' ' + weight + '">';
		popup += '<h2>' + species + ' ' + weight + '<a class="remove-marker pull-right" onclick="$(document).trigger(\'removeMarker\', {\'type\' : \'catch-marker\'});"><i class="fa fa-2x fa-times"></i></a></h2>';
		
		popup += '<div class="decoration"></div>';

		popup += '<div class="catch-meta">';
	if (catcher !== "-") {
		popup += '<span class="property">Kalastaja:</span><span class="catcher value">'+ catcher +'</span><br>';
	}
		popup += '<span class="property">Päivämäärä:</span><span class="date value">'+ date +'</span><br>';
		popup += '<span class="property">Sää pyyntipäivänä:</span><span class="weather value">';
		popup += temp;
	if (rain > 0 && rain > snow) {

		popup += '<img src="images/weather/4.png" />';
	}
	else if (snow > 0) {

		popup += '<img src="images/weather/5.png" />';
	}
	else if (temp !== "-") {

		popup += '<img src="images/weather/3.png" />';
	}
	popup += '</span>';
	popup += '</div>';
	if (!!data.img && data.img.indexOf("http") !== -1) {

		popup += '<div class="catch-image"><img src="'+ data.img +'" /></div>';
	}
	else if (!!data.img && data.img.indexOf("http") === -1) {

		popup += '<div class="catch-image" data-id="'+data.img+'"><i class="fa fa-spinner fa-pulse"></i></div>';
	}

		popup += '<div class="decoration"></div>';

		popup += '<div class="footer-socials">';
		popup += '<a onclick="$(document).trigger(\'markerShare\', {to : \'fb\'})" class="footer-icon footer-facebook" href="#"><i class="fa fa-facebook"></i></a>';
		popup += '<a onclick="$(document).trigger(\'markerShare\', {to : \'twitter\'})" href="#" class="footer-icon footer-twitter"><i class="fa fa-twitter"></i></a>';
		popup += '<a onclick="$(document).trigger(\'markerShare\', {to : \'email\'})" class="footer-icon footer-email" href="#"><i class="fa fa-envelope"></i></a>';
		popup += '<br><div class="form-group hidden share-email-address"><input type="text" placeholder="Ystäväsi sähköpostiosoite" name="share-email-address" /><a href="#" onclick="$(document).trigger(\'markerShare\', {to : \'email\'})" class="button button-yellow submit"><i class="fa fa-share"></i></a></div>';
		popup += '</div>';

		popup += '<div class="decoration"></div>';

		popup += '<div class="popup-buttons">';
	    popup += '<button onclick="$(document).trigger(\'showWeather\', {lat : \'' + data.lat + '\',  lon : \'' + data.lon + '\'})" class="button button-blue button-wide">Katso sää</button>';
	    popup += '<button disabled data-status="working" class="story-link button button-light"><i class="fa fa-spinner fa-spin"></i>Lue saalistarina</button>';
		popup += '</div>';

		popup += '</div>';

	marker.bindPopup(popup);
	return marker;
}

KalakeliData.prototype.getSessionCookie = function()
{
	var cookie_data = document.cookie;
	var cookies = cookie_data.split(';');
	var found = false;
	var profile_found = false;
	for (var cookie in cookies) {

		cookie_parts = cookies[cookie].split('=');
		if ($.trim(cookie_parts[0]) === 'kalakeli_session') {
			found = true;
			profile_found = true;
			var session_data = $.parseJSON($.trim(cookie_parts[1]));
			if (session_data.persistent) {

				var cookie_string = "kalakeli_session=" + $.trim(cookie_parts[1]) + "; path=/;";
				var expiration_date = new Date();
				expiration_date.setFullYear(expiration_date.getFullYear() + 1);
				cookie_string += "expires=" + expiration_date.toGMTString();
				document.cookie = cookie_string;
			}
			$(document).trigger('userData', session_data);
			$(document).trigger('userSignedIn', session_data);
		}
		else if ($.trim(cookie_parts[0]) === 'kalakeli_new') {

			found = true;
			var cookie_string = "kalakeli_new=" + $.trim(cookie_parts[1]) + "; path=/;";
			var expiration_date = new Date();
			expiration_date.setFullYear(expiration_date.getFullYear() + 1);
			cookie_string += "expires=" + expiration_date.toGMTString();
			document.cookie = cookie_string;
		}
	}
	if (!found) {

		var cookie_string = "kalakeli_new=false; path=/;";
		var expiration_date = new Date();
		expiration_date.setFullYear(expiration_date.getFullYear() + 1);
		cookie_string += "expires=" + expiration_date.toGMTString();
		document.cookie = cookie_string;

		$(document).trigger("newUser");
	}
}

KalakeliData.prototype.register = function(data)
{
	var _this = this;

	if (typeof data !== "object") data = {}; 
	var email = data.email || this.User.email;

	$.ajax(this.Blackbox.register(email)).done(function(data) {

		if (data === 'REGISTRATION_FAILED') {

			$(document).trigger("registrationFailed", {"data" : data});
		}
		else {

			if (typeof 'data' !== 'object') {

				try {
					data = $.parseJSON(data);
				}
				catch(err) {
					$(document).trigger("registrationFailed", {"data" : data})
				}
			}
		}

		$(document).trigger("userData", {"email" : data.email, "password" : data.default_password});
		$(document).trigger("registrationSuccess", true);
	});
}

KalakeliData.prototype.getUserLocation = function()
{
	var _this = this;
    if (!!navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position){
        	_this.User.geoLocation = position;
        	$(document).trigger("userGeoLocation", {"position" : _this.User.geoLocation});
        }, function onError(error) {
        	// no location
		});
    }
    else {
    	$.ajax(_this.GeoIP.getLocation())
    		.done(function(response) {

    			if (typeof response !== "object") {

    				try {
    					response = $.parseJSON(response);
    				}
    				catch (err) {
    					$(document).trigger("noGeoLocation");
    				}
    			}
    			_this.User.geoLocation.coords = {'latitude' : response.lat, 'longitude' : response.lon};
    			$(document).trigger("userGeoLocation", {"position" : _this.User.geoLocation});
    		});
    }
}

KalakeliData.prototype.getMapFeatures = function()
{
	var _this = this;

	var mapBounds = _this.Mapbox.map.getBounds();
	var deltaLat = Math.abs(mapBounds.getSouth() - mapBounds.getNorth());
	var deltaLng = Math.abs(mapBounds.getWest() - mapBounds.getEast());
	var south = mapBounds.getSouth() - deltaLat;
	var west = mapBounds.getWest() - deltaLng;
	var north = mapBounds.getNorth() + deltaLat;
	var east = mapBounds.getEast() + deltaLng;
	var featureUrl = _this.Blackbox.catches(south, west, north, east);

	$.ajax(_this.Blackbox.catches(south, west, north, east))
		.done(function(data) {

			if (typeof data !== "object") {

				try {
					data = $.parseJSON(data);
				}
				catch(err) {
					return false;
				}
			}

			_this.Mapbox.catchBounds = {
				'south' : south,
				'west' : west,
				'north' : north,
				'east' : east,
			}
			return $(document).trigger("catches", {"features" : data});
	});
}

KalakeliData.prototype.checkCatches = function()
{
	var _this = this;
	var currentBounds = this.Mapbox.map.getBounds();

	if (this.Mapbox.map.getZoom() < 8 || typeof _this.Mapbox.catchBounds === "undefined") return true;

	if (currentBounds.getSouth() < this.Mapbox.catchBounds.south ||
		currentBounds.getWest() < this.Mapbox.catchBounds.west ||
		currentBounds.getNorth() > this.Mapbox.catchBounds.north ||
		currentBounds.getEast() > this.Mapbox.catchBounds.east) {

		this.getMapFeatures();
	}
}

KalakeliData.prototype.signUserIn = function()
{
	var _this = this;

	$.ajax(this.Blackbox.signin(this.User.email, this.User.password)).done(function(data) {

		if (data === 'AUTHENTICATION_FAILED') {

			$(document).trigger('userSignedInError');
		}
		else {

			var data = $.parseJSON(data);
			$(document).trigger('userData', data);

			var cookie_string = "kalakeli_session=" + JSON.stringify(_this.User) + "; path=/;";
			if (_this.User.persistent) {

				var expiration_date = new Date();
				expiration_date.setFullYear(expiration_date.getFullYear() + 1);
				cookie_string += "expires=" + expiration_date.toGMTString();
			}
			document.cookie = cookie_string;
			$(document).trigger('userSignedIn', data);
		}
	});
}

KalakeliData.prototype.getUserProfile = function()
{
	var _this = this;

	$.ajax(this.Blackbox.getprofile()).done(function(profile) {

		if (!!profile) {

			if (typeof profile !== 'object') {

				try {
					profile = $.parseJSON(profile);
				}
				catch(err) {

					return false;
				}
			}

			if (profile.constructor === Array) {
				profile = {'places' : []};
			}
			else {
				for (var i in profile.places) {
					if (!profile.places[i]) {
						delete profile.places[i];
					}
				}
			}
			if (typeof profile.places === "undefined") {

				profile.places = [];
			}
			_this.User.places = profile.places;
			if (typeof profile.emailNotifications !== "undefined") {

				_this.User.emailNotifications = profile.emailNotifications;
			}

			$(document).trigger('userProfile', _this.User);
		}
	});
}

KalakeliData.prototype.setUserProfile = function()
{
	var _this = this;
	$.ajax(this.Blackbox.setprofile()).done(function(data) {

		
	});
}

KalakeliData.prototype.getUserCatchlist = function(options)
{
	var _this = this;

	$.ajax(this.Blackbox.listcatch(options)).done(function(data) {

		var catchlist = data;
		if (typeof catchlist !== 'object') {

			try {

				catchlist = $.parseJSON(catchlist);
			}
			catch(err) {

				return false;
			}
		}
		_this.User.catchlist = catchlist;
		$(document).trigger('userCatchlist', {'catchlist' : catchlist});
	});
}

KalakeliData.prototype.getWeatherData = function(coords)
{
	if (typeof coords === undefined) {
		coords = {};
	}
	var _this = this;
    $.ajax({
    	url : this.Weather.url(coords.lat, coords.lon),
    	crossDomain : true,
    	dataType : 'json'

	}).done(function(data) {

		if (typeof data !== 'object') {

			try {
	    		data = $.parseJSON(data);
	    	}
	    	catch(err) {

	    		return false;
	    	}
	    }

	    return $(document).trigger("weatherData", {"weatherData" : data})
    });
}

KalakeliData.prototype.searchLocation = function(data)
{
	var _this = this;

	if (typeof data === "undefined") return false;
	if (!data.place) return false;

	$.ajax(this.Blackbox.geocode(data.place, 10)).done(function(data) {

		if (typeof data !== "object") {

			try {
				data = $.parseJSON(data);
			}
			catch(err) {
				return false;
			}
		}

		$(document).trigger("locationSearchResults", {"data" : data});
	});
}

KalakeliData.prototype.updateCompass = function(angle)
{
	var _this = this;

	var angle = 360 - Math.round(parseFloat(angle));
	var rad = angle * (Math.PI/180);
	var cos = Math.cos(rad);
	var sin = Math.sin(rad);

	var styles = {
		"-webkit-transform" : "rotateZ("+angle+"deg)",
		"-moz-transform" : "rotateZ("+angle+"deg)",
		"-ms-transform" : "rotateZ("+angle+"deg)",
		"-o-transform" : "rotateZ("+angle+"deg)",
		"transform" : "rotateZ("+angle+"deg)",
		"-ms-filter" : "progid:DXImageTransform.Microsoft.Matrix(M11=" + cos + ", M12=" + (-1 * sin) + ", M21=" + sin + ", M22=" + cos + ", SizingMethod='auto expand')"
	}

	$("#compass").css(styles);
}

AppData = new KalakeliData();